<div class="block_footer">
	<div class="fmap">
		<div class="fmap_desc">
			<p>109240,</p>
			<p>Россия, г. Москва,</p>
			<p>ул. Верхняя Радищевская,</p>
			<p>д. 16, стр. 2-3</p>
		</div><!-- fmap_desc -->
	</div><!-- fmap -->
	<div class="fphone">
		<div class="fphone_desc">
			<p><a href="tel:+74959090777">8 495 90 90 777</a></p>
		</div><!-- fmap_desc -->
	</div><!-- fmap -->
	<div class="femail">
		<div class="femail_desc">
			<p>Для клиентов НПФ: <br><a href="mailto:customer_service@safmarpensions.ru">customer_service@safmarpensions.ru</a></p>
			<p>По общим вопросам: <br><a href="mailto:info@safmarpensions.ru">info@safmarpensions.ru</a></p>
		</div><!-- fmap_desc -->
	</div><!-- fmap -->
	<div class="f_logo"><img src="/upload/img/logo_footer3.png" height="99" width="76"></div>
</div><!-- logo_footer -->




<!-- Google analytics -->
<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-71557739-1', 'auto');
    ga('send', 'pageview');

</script>
<!-- /Google analytics -->

<!-- Yandex.Metrika counter -->
<script type="text/javascript">
        (function (d, w, c) {
                (w[c] = w[c] || []).push(function() {
                        try {
                                w.yaCounter34785960 = new Ya.Metrika({
                                        id:34785960,
                                        clickmap:true,
                                        trackLinks:true,
                                        accurateTrackBounce:true,
                                        webvisor:true
                                });
                        } catch(e) { }
                });

                var n = d.getElementsByTagName("script")[0],
                        s = d.createElement("script"),
                        f = function () { n.parentNode.insertBefore(s, n); };
                s.type = "text/javascript";
                s.async = true;
                s.src = "https://mc.yandex.ru/metrika/watch.js";

                if (w.opera == "[object Opera]") {
                        d.addEventListener("DOMContentLoaded", f, false);
                } else { f(); }
        })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/34785960" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
