<?if(!CModule::IncludeModule("iblock"))
    return;?>

    <div class="header">
    	<div class="header_left">
    		<div class="logo">
    			<a href="/"><img src="/upload/img/logo_safmar2.png" height="100" width="224"></a>
    		</div>
    	</div><!-- header_right -->
    	<div class="header_right">
    		<div class="phone">
                        <?$APPLICATION->IncludeComponent(
                                "bitrix:main.include", 
                                ".default", 
                                array(
                                        "AREA_FILE_SHOW" => "file",
                                        "AREA_FILE_SUFFIX" => "inc",
                                        "AREA_FILE_RECURSIVE" => "Y",
                                        "EDIT_TEMPLATE" => "standard.php",
                                        "PATH" => "/include/phone.php"
                                ),
                                false
                        );?>                        
    		</div><!-- phone -->
    	</div><!-- header_right -->
    </div><!-- header -->

	<div class="wrapper">
        <?
            $uri = explode("/", $_SERVER['REQUEST_URI']);
            if($uri[1] === "services" || $uri[1] === "partners" || $uri[1] === "documents" || $uri[1] === "contact")
                $postfix = "_".$uri[1];
            else
                $postfix = "";
        ?>
		<div class="block_img<?=$postfix?>">
			<h2><?$APPLICATION->ShowProperty("h1");?></h2>
		</div><!-- block_img -->

		<?$APPLICATION->IncludeComponent("bitrix:menu", "main_menu", Array(
			"ROOT_MENU_TYPE" => "top",	// Тип меню для первого уровня
			"MAX_LEVEL" => "0",	// Уровень вложенности меню
		), false );?>