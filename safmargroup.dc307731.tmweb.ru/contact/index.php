<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("h1", "Присоединяйтесь");
$APPLICATION->SetTitle("Сафмар");
?>

<div class="block_desc_contact">
	<div class="con_phone">
		<p><a href="tel:+74959090777">8 495 90 90 777</a></p>
	</div><!-- con_phone -->
	<div class="con_email">
		<div class="con_map_desc">
			<p>Для клиентов НПФ<br><a href="mailto:customer_service@safmarpensions.ru">customer_service@safmarpensions.ru</a></p>
			
			<p>По общим вопросам<br><a href="mailto:info@safmarpensions.ru">info@safmarpensions.ru</a></p>
		</div>
	</div><!-- con_phone -->
	<div class="con_map">
		<div class="con_map_desc">
			<p>109240, Россия</p>
			<p>г. Москва, </p>
			<p>ул. Верхняя Радищевская, </p>
			<p>д. 16, стр. 2-3</p>
		</div>
	</div><!-- con_phone -->
</div>

<div class="map_contact">
    <div id="map_contact" style="width: 100%; height: 450px"></div>
</div>

<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>