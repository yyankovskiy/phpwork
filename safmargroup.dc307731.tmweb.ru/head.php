<meta charset="UTF-8">
<link rel="shortcut icon" href="/img/favicon.ico" type="image/x-icon">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" href="/css/style.css" type="text/css" />
<link rel="stylesheet" href="/css/jquery.fancybox.css" type="text/css" />
<script type="text/javascript" src="/js/jquery-1.12.0.min.js"></script>
<script type="text/javascript" src="/js/jquery.fancybox.pack.js"></script>
<script type="text/javascript" src="/js/action.js"></script>
<script type="text/javascript" src="/js/map_contact.js"></script>
<script type="text/javascript" src="/js/circles.min.js"></script>
<script type="text/javascript">
    jQuery(document).ready(function () {
        jQuery(".fancybox").fancybox({
     fitToView : false,
     speedIn: '10000', 
     speedOut: '10000',
     margin: [0, 0, 0, 0],
     autoSize  : false,
     'frameWidth': 10,
     'frameHeight':495,
     'overlayShow':true,
     'hideOnContentClick':false,
     'type':'iframe',
     iframe: {
            preload: false // fixes issue with iframe and IE
            }
        });
    });
</script>
