<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("h1", "Создавайте уверенное будущее");
$APPLICATION->SetTitle("Сафмар");
?><div class="block_desc">
	<div class="block_desc_left">
		<h4>Пенсионный администратор</h4>
		<p style="text-align: justify;">
			 Пенсионный администратор ООО «САФМАР ПЕНСИИ» специализируется на ведении пенсионных счетов застрахованных лиц негосударственных пенсионных фондов (НПФ) в соответствии с Федеральным законом от 07.05.1998 г. №75-ФЗ «О&nbsp;негосударственных пенсионных фондах».
		</p>
		 <!-- <p>Пенсионный администратор «САФМАР ПЕНСИИ» специализируется<br /> на ведении пенсионных счетов накопительной пенсии застрахованных лиц негосударственных пенсионных фондов (НПФ)<br /> в соответствии с п.2 ст.8 Федерального закона от 07.05.1998 г.<br /> № 75-ФЗ «О негосударственных пенсионных фондах», включая услуги по формированию пенсии и выплате накопленной части пенсии наследникам.</p> -->
	</div>
	 <!-- block_desc_left -->
</div>
 <!-- block_desc --> <?$APPLICATION->IncludeComponent(
	"bitrix:news.list",
	"about_company",
	Array(
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"ADD_SECTIONS_CHAIN" => "Y",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"CHECK_DATES" => "Y",
		"COMPONENT_TEMPLATE" => "about_company",
		"DETAIL_URL" => "",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"DISPLAY_DATE" => "N",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "N",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"FIELD_CODE" => array("",""),
		"FILTER_NAME" => "",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"IBLOCK_ID" => "1",
		"IBLOCK_TYPE" => "CONTENT",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
		"INCLUDE_SUBSECTIONS" => "N",
		"MESSAGE_404" => "",
		"NEWS_COUNT" => "20",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_TITLE" => "Новости",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"PREVIEW_TRUNCATE_LEN" => "",
		"PROPERTY_CODE" => array("","LINK_DOCUMENT",""),
		"SET_BROWSER_TITLE" => "N",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "N",
		"SET_META_KEYWORDS" => "N",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "N",
		"SHOW_404" => "N",
		"SORT_BY1" => "ACTIVE_FROM",
		"SORT_BY2" => "SORT",
		"SORT_ORDER1" => "DESC",
		"SORT_ORDER2" => "DESC"
	)
);?>
<div class="block_desc" style="margin: 0 60px; font-size: 18px;">
	<p style="margin-bottom: 15px;">
		 Приказом № 210 от 05.11.2015 ФЕДЕРАЛЬНОЙ СЛУЖБЫ ПО НАДЗОРУ В СФЕРЕ СВЯЗИ, ИНФОРМАЦИОННЫХ ТЕХНОЛОГИЙ И МАССОВЫХ КОММУНИКАЦИЙ (Роскомнадзор) ООО «САФМАР ПЕНСИИ» было внесено в реестр Операторов Персональных данных под номером 77-15-003874.
	</p>
	<p>
		 Дополнительную информацию вы можете найти по ссылке: <a href="http://rkn.gov.ru/personal-data/register/?id=77-15-003874" target="_blank" rel="nofollow">http://rkn.gov.ru/personal-data/register/?id=77-15-003874</a>.
	</p>
</div>
<br><? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>