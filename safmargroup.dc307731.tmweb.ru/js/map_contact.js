//Map___________________________________________________________________________
jQuery(function($) {
	// Asynchronously Load the map API 
	var script = document.createElement('script');
	script.src = "http://maps.googleapis.com/maps/api/js?sensor=false&callback=initialize";
	document.body.appendChild(script);
});
			
function initialize() {
	var map;      
	var my_map = 'custom_style';
	var bounds = new google.maps.LatLngBounds();
	var featureOpts =[
		{
			"featureType": "landscape",
			"stylers": [
				{
					"saturation": -100
				},
				{
					"lightness": 65
				},
				{
					"visibility": "on"
				}
			]
		},
		{
			"featureType": "poi",
			"stylers": [
				{
					"saturation": -100
				},
				{
					"lightness": 51
				},
				{
					"visibility": "simplified"
				}
			]
		},
		{
			"featureType": "road.highway",
			"stylers": [
				{
					"saturation": -100
				},
				{
					"visibility": "simplified"
				}
			]
		},
		{
			"featureType": "road.arterial",
			"stylers": [
				{
					"saturation": -100
				},
				{
					"lightness": 30
				},
				{
					"visibility": "on"
				}
			]
		},
		{
			"featureType": "road.local",
			"stylers": [
				{
					"saturation": -100
				},
				{
					"lightness": 40
				},
				{
					"visibility": "on"
				}
			]
		},
		{
			"featureType": "transit",
			"stylers": [
				{
					"saturation": -100
				},
				{
					"visibility": "simplified"
				}
			]
		},
		{
			"featureType": "administrative.province",
			"stylers": [
					{
							"visibility": "off"
					}
			]
		},
		{
			"featureType": "water",
			"elementType": "labels",
			"stylers": [
				{
					"visibility": "on"
				},
				{
					"lightness": -25
				},
				{
					"saturation": -100
				}
			]
		},
		{
			"featureType": "water",
			"elementType": "geometry",
			"stylers": [
				{
					"hue": "#ffff00"
				},
				{
					"lightness": -25
				},
				{
					"saturation": -97
				}
			] 
		}
	]
	var Moscow = new google.maps.LatLng(55.7433528,37.6506263);
	var mapOptions = {
		zoom: 18,
		center: Moscow,
		panControl: false,
		zoomControl: false,
		scaleControl: false,
		streetViewControl: false,
		overviewMapControl: false,
		mapTypeControl: false,
		scrollwheel: false,
		mapTypeControlOptions: {
			mapTypeIds: [google.maps.MapTypeId.ROADMAP, my_map]
		},
		mapTypeId: my_map
	}; // end mapOptions;
					
	var map = new google.maps.Map(document.getElementById('map_contact'), mapOptions);
	map.setTilt(45);
	var markers = [];

	// Display multiple markers on a map
	var infoWindow = new google.maps.InfoWindow(), marker, i;

	// Loop through our array of markers & place each one on the map  
	for( i = 0; i < markers.length; i++ ) {
		var position = new google.maps.LatLng(markers[i][1], markers[i][2]);
		bounds.extend(position);
		marker = new google.maps.Marker({
			position: position,
			map: map,
			title: markers[i][0]
		});
		// Allow each marker to have an info window    
		google.maps.event.addListener(marker, 'click', (function(marker, i) {
			return function() {
				infoWindow.setContent(infoWindowContent[i][0]);
				infoWindow.open(map, marker);
			}
		})(marker, i));  
		// Automatically center the map fitting all markers on the screen
		//map.fitBounds(bounds);
	} 
	// Override our map zoom level once our fitBounds function runs (Make sure it only runs once)
	// var boundsListener = google.maps.event.addListener((map), 'bounds_changed', function(event) {
	//     this.setZoom(10);
	//     google.maps.event.removeListener(boundsListener);
	// });
	var styledMapOptions = {
		name: 'Custom Style'
	};//end styledMapOptions
	var customMapType = new google.maps.StyledMapType(featureOpts, styledMapOptions);
	map.mapTypes.set(my_map, customMapType);


	//------------------------------------------------------------------------------
	var officeImage = new google.maps.MarkerImage('/upload/img/marker_map.png',
		new google.maps.Size(41,49),
		new google.maps.Point(0,0),
		new google.maps.Point(50,50)
	);
	var officeShadow = new google.maps.MarkerImage('/upload/img/marker_map.png',
		new google.maps.Size(41,49),
		new google.maps.Point(0,0),
		new google.maps.Point(65, 50));
	//------------------------------------------------------------------------------
	var office = [
		['Moscow ', 55.7433528,37.6506263]
	]; 
	//------------------------------------------------------------------------------
	//==============================================================================
	//------------------------------------------------------------------------------
	// Info Window Content
 	var infoWindowContentsOffice = [
		['<div class="info_content">' + '<h3>Геодезия и Кадастр</h3>' + '<p>в Москве и Московской области, включая и коттеджные посёлки, и многоквартирные дома, и большие офисные центры.</p>' + '</div>']
	];
	//==============================================================================
	//------------------------------------------------------------------------------
	var infoOffice = new google.maps.InfoWindow(), office, i;
	// Loop through our array of markers & place each one on the map  
	var position = new google.maps.LatLng(office[i][1], office[i][2]);
	//bounds.extend(position);
	office = new google.maps.Marker({
		position: position,
		map: map,
		icon: officeImage,
		shadow: officeShadow,
		title: office[i][0]
	});
	// Allow each marker to have an info window    
	google.maps.event.addListener(office, 'click', (function(office, i) {
		return function() {
			infoOffice.setContent(infoWindowContentsOffice[i][0]);
			infoOffice.open(map, office);
		}
	})(office, i));
	//------------------------------------------------------------------------------
} // end initialize()

//End Map_______________________________________________________________________



