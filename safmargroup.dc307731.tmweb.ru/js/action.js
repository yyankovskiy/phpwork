$(document).ready(function() {

$('body').on('click','span.view_services', function(){
    $('.accordion-section-content').stop(true,true);
    var obj = $(this);
    if(obj.text() == 'Смотреть все услуги'){
        obj.text('Скрыть все услуги');
        $('.accordion-section-content').each(function(){
            if($(this).css('display')==='none'){
                $(this).addClass('open');
                $(this).toggle('slow');
            }
        });
        $('.accordion-section-title').each(function() {
            if(!$(this).hasClass('active'))
            {
                $(this).addClass('active');
                $(this).find('div').addClass('active_arrow');
            }
        });
    }else{
        obj.text('Смотреть все услуги');
        $('.accordion-section-content').each(function(){
            if($(this).css('display')==='block'){
                $(this).removeClass('open');
                $(this).toggle('slow');
            };
        }); 
        $('.accordion-section-title').each(function() {
            if($(this).hasClass('active'))
            {
                $(this).find('div').removeClass('active_arrow');
                $(this).removeClass('active');
            }
        });   
    }
});

$('body').on('click','span.accordion-section-title', function(){
    $('.accordion-section-content').stop(true,true);
    var obj = $(this);
    var key = true;
    if(obj.hasClass('active')){
        key = false;
    }
    
    if($('span.view_services').text()=='Скрыть все услуги'){
        $('span.view_services').text('Смотреть все услуги');
    }                
    
    if($('span.accordion-section-title.active')){
        $('span.accordion-section-title.active').removeClass('active');
        $('div.arrow.active_arrow').removeClass('active_arrow');
    }
    
    $('.accordion-section-content').each(function(){
        if($(this).css('display')==='block'){
                $(this).removeClass('open');
                $(this).toggle('slow');
        }
    });
    
    if(key){
        if(obj.parent().next('.accordion-section-content').is('.open')){
                obj.removeClass('active');
                obj.find('div').removeClass('active_arrow');
                obj.parent().next('.accordion-section-content').removeClass('open');
        }else{
                obj.addClass('active'); 
                obj.find('div').addClass('active_arrow');
                obj.parent().next('.accordion-section-content').addClass('open');
        }
        obj.parent().next('.accordion-section-content').toggle('slow');
    }
});

});