<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<ul class="charter">
			<?foreach($arResult["ITEMS"] as $arItem):?>
				<li>
					<div class="desc">
						<a href="<?=$arItem["DISPLAY_PROPERTIES"]["LINK_DOCUMENT"]["FILE_VALUE"]["SRC"]?>" target="_blank" class="vs"><?=$arItem["PREVIEW_TEXT"]?></a>
						<a href="<?=$arItem["DISPLAY_PROPERTIES"]["LINK_DOCUMENT"]["FILE_VALUE"]["SRC"]?>" target="_blank" class="hd"><?=$arItem["NAME"]?></a>
					</div>
				</li>
			<?endforeach;?>
		</ul>
	</div>
</div>
				