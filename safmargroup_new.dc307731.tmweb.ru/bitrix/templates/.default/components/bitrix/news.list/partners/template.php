<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<div class="tabs_partners">
			<ul class="part">
				<?foreach ($arResult["ITEMS"] as $key => $arItem){?>
					<?if($key != 0){?>
						<li data-index="<?=$key+1?>">
					<?}else{?>
						<li data-index="<?=$key+1?>" class="active">
					<?}?>
					<?if(empty($arItem["PREVIEW_TEXT"])){?>
						<h5><?=$arItem["NAME"]?></h5>
					<?}else{?>
						<h5 class="vs"><?=$arItem["PREVIEW_TEXT"]?></h5>
						<h5 class="hd"><?=$arItem["NAME"]?></h5>
					<?}?>
					</li>
				<?}?>
			</ul>
		</div>
	</div>
</div>


<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<?foreach ($arResult["ITEMS"] as $key => $arItem){?>
			<?if($key != 0){
				$class_name = ($key+1)."";
			}else{
				$class_name = ($key+1)." active";
			}?>
			
			<div class="tabs__content tc<?=$class_name?>" data-index="<?=$key+1?>">
				<?if(!empty($arItem["PROPERTIES"]["FOND"]["VALUE"]) || 
					!empty($arItem["PROPERTIES"]["ZASTR_LIC"]["VALUE"]) || 
					!empty($arItem["PROPERTIES"]["KOL_UCHASTNIKOV"]["VALUE"]))
				{?>
					<ul class="number_participants">
						<?if(!empty($arItem["PROPERTIES"]["KOL_UCHASTNIKOV"]["VALUE"])){
							$KOL_UCHASTNIKOV = number_format($arItem["PROPERTIES"]["KOL_UCHASTNIKOV"]["VALUE"], 0, ",", " ");?>
							<li>
								<div class="desc" id="pcircle<?=$key+1?>1" data-text="Количество участников" data-mesure="чел" data-value="<?=$arItem["PROPERTIES"]["KOL_UCHASTNIKOV"]["VALUE"]?>">
									<div class="circle" id="pcircles-<?=$key+1?>1"></div>
									<!--<p>Количество участников, <span>чел</span></p>
									<p class="number"><?=$KOL_UCHASTNIKOV?></p>
									<div class="crc"></div>-->
								</div>
							</li>
						<?}?>

						<?if(!empty($arItem["PROPERTIES"]["ZASTR_LIC"]["VALUE"])){
							$ZASTR_LIC = number_format($arItem["PROPERTIES"]["ZASTR_LIC"]["VALUE"], 0, ",", " ");?>
							<li>
								<div class="desc" id="pcircle<?=$key+1?>2" data-text="Количество застрахованных лиц" data-mesure="чел" data-value="<?=$arItem["PROPERTIES"]["ZASTR_LIC"]["VALUE"]?>">
								<div class="circle" id="pcircles-<?=$key+1?>2"></div>
									<!--<p>Количество застрахованных лиц, <span>чел</span></p>
									<p class="number"><?=$ZASTR_LIC?></p>
									<div class="crc"></div>-->
								</div>
							</li>
						<?}?>

						<?if(!empty($arItem["PROPERTIES"]["FOND"]["VALUE"])){
							$FOND = number_format($arItem["PROPERTIES"]["FOND"]["VALUE"], 0, ",", " ");?>
							<li>
								<div class="desc" id="pcircle<?=$key+1?>3" data-text="Имущество фонда" data-mesure="руб" data-value="<?=$arItem["PROPERTIES"]["FOND"]["VALUE"]?>">
									<div class="circle" id="pcircles-<?=$key+1?>3"></div>
									<!--<p>Имущество фонда, <span>руб</span></p>
									<p class="number"><?=$FOND?></p>
									<div class="crc"></div>-->
								</div>
							</li>
						<?}?>
					</ul>
				<?}?>
				<?if(!empty($arItem["PROPERTIES"]["ID_VIPLOT"]["VALUE"]) || !empty($arItem["PROPERTIES"]["ID_CHANGE"]["VALUE"])){?>
					<div class="funded_pension">
						<div class="container">
							<ul class="all_pack">
					            <li>
									<div class="row">
										<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
											<ul class="fun">
												<li>Выплата накопительной пенсии</li>
											</ul>
										</div>
									</div>
									<div class="row">
										<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
											<?global $arrFilter;
											$arrFilter = array("SECTION_ID"=> $arItem["PROPERTIES"]["ID_VIPLOT"]["VALUE"]);
											$APPLICATION->IncludeComponent( "bitrix:news.list", "documents", array(
												"ACTIVE_DATE_FORMAT" => "d.m.Y",
												"ADD_SECTIONS_CHAIN" => "N",
												"AJAX_MODE" => "N",
												"AJAX_OPTION_ADDITIONAL" => "",
												"AJAX_OPTION_HISTORY" => "N",
												"AJAX_OPTION_JUMP" => "N",
												"AJAX_OPTION_STYLE" => "Y",
												"CACHE_FILTER" => "N",
												"CACHE_GROUPS" => "Y",
												"CACHE_TIME" => "36000000",
												"CACHE_TYPE" => "A",
												"CHECK_DATES" => "Y",
												"COMPONENT_TEMPLATE" => "documents",
												"DETAIL_URL" => "",
												"DISPLAY_BOTTOM_PAGER" => "N",
												"DISPLAY_DATE" => "N",
												"DISPLAY_NAME" => "Y",
												"DISPLAY_PICTURE" => "N",
												"DISPLAY_PREVIEW_TEXT" => "Y",
												"DISPLAY_TOP_PAGER" => "N",
												"FIELD_CODE" => array(
													0 => "",
													1 => "",
												),
												"FILTER_NAME" => "arrFilter",
												"HIDE_LINK_WHEN_NO_DETAIL" => "N",
												"IBLOCK_ID" => "4",
												"IBLOCK_TYPE" => "CONTENT",
												"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
												"INCLUDE_SUBSECTIONS" => "Y",
												"MESSAGE_404" => "",
												"NEWS_COUNT" => "999",
												"PAGER_BASE_LINK_ENABLE" => "N",
												"PAGER_DESC_NUMBERING" => "N",
												"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
												"PAGER_SHOW_ALL" => "N",
												"PAGER_SHOW_ALWAYS" => "N",
												"PAGER_TEMPLATE" => ".default",
												"PAGER_TITLE" => "Новости",
												"PARENT_SECTION" => "",
												"PARENT_SECTION_CODE" => "",
												"PREVIEW_TRUNCATE_LEN" => "",
												"PROPERTY_CODE" => array("LINK_DOC", "LINK_FORM_DOC"),
												"SET_BROWSER_TITLE" => "N",
												"SET_LAST_MODIFIED" => "N",
												"SET_META_DESCRIPTION" => "N",
												"SET_META_KEYWORDS" => "N",
												"SET_STATUS_404" => "N",
												"SET_TITLE" => "N",
												"SHOW_404" => "N",
												"SORT_BY1" => "SORT",
												"SORT_BY2" => "TIMESTAMP_X",
												"SORT_ORDER1" => "DESC",
												"SORT_ORDER2" => "DESC"
											), false );?>
										</div>
									</div>
								</li>
								<li>
									<div class="row">
										<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
											<ul class="fun">
												<li>Изменение персональных данных</li>
											</ul>
										</div>
									</div>
									<div class="row">
										<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
											<?global $arrFilter;
											$arrFilter = array("SECTION_ID"=> $arItem["PROPERTIES"]["ID_CHANGE"]["VALUE"]);
											$APPLICATION->IncludeComponent( "bitrix:news.list", "documents", array(
												"ACTIVE_DATE_FORMAT" => "d.m.Y",
												"ADD_SECTIONS_CHAIN" => "N",
												"AJAX_MODE" => "N",
												"AJAX_OPTION_ADDITIONAL" => "",
												"AJAX_OPTION_HISTORY" => "N",
												"AJAX_OPTION_JUMP" => "N",
												"AJAX_OPTION_STYLE" => "Y",
												"CACHE_FILTER" => "N",
												"CACHE_GROUPS" => "Y",
												"CACHE_TIME" => "36000000",
												"CACHE_TYPE" => "A",
												"CHECK_DATES" => "Y",
												"COMPONENT_TEMPLATE" => "documents",
												"DETAIL_URL" => "",
												"DISPLAY_BOTTOM_PAGER" => "N",
												"DISPLAY_DATE" => "N",
												"DISPLAY_NAME" => "Y",
												"DISPLAY_PICTURE" => "N",
												"DISPLAY_PREVIEW_TEXT" => "Y",
												"DISPLAY_TOP_PAGER" => "N",
												"FIELD_CODE" => array(
													0 => "",
													1 => "",
												),
												"FILTER_NAME" => "arrFilter",
												"HIDE_LINK_WHEN_NO_DETAIL" => "N",
												"IBLOCK_ID" => "4",
												"IBLOCK_TYPE" => "CONTENT",
												"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
												"INCLUDE_SUBSECTIONS" => "Y",
												"MESSAGE_404" => "",
												"NEWS_COUNT" => "999",
												"PAGER_BASE_LINK_ENABLE" => "N",
												"PAGER_DESC_NUMBERING" => "N",
												"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
												"PAGER_SHOW_ALL" => "N",
												"PAGER_SHOW_ALWAYS" => "N",
												"PAGER_TEMPLATE" => ".default",
												"PAGER_TITLE" => "Новости",
												"PARENT_SECTION" => "",
												"PARENT_SECTION_CODE" => "",
												"PREVIEW_TRUNCATE_LEN" => "",
												"PROPERTY_CODE" => array("LINK_DOC", "LINK_FORM_DOC"),
												"SET_BROWSER_TITLE" => "N",
												"SET_LAST_MODIFIED" => "N",
												"SET_META_DESCRIPTION" => "N",
												"SET_META_KEYWORDS" => "N",
												"SET_STATUS_404" => "N",
												"SET_TITLE" => "N",
												"SHOW_404" => "N",
												"SORT_BY1" => "SORT",
												"SORT_BY2" => "TIMESTAMP_X",
												"SORT_ORDER1" => "DESC",
												"SORT_ORDER2" => "DESC"
											), false );?>
										</div>
									</div>
								</li>
							</ul>
						</div>
					</div>
				<?}?>
			</div>
		<?}?>
	</div>
</div>