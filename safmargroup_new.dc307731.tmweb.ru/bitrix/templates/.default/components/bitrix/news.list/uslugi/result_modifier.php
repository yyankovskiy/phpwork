<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$arOrder = array( "SORT" => "ASC", "ACTIVE_FROM" => "DESC");
$arFilter = Array("IBLOCK_ID" => $arParams["IBLOCK_ID"], "IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"]);
$arSelect = Array("ID", "NAME", "DESCRIPTION", 'CODE');
$res = CIBlockSection::GetList($arOrder, $arFilter, false, $arSelect, Array("nPageSize" => $arParams["NEWS_COUNT"]));
while($ob = $res->GetNextElement())
{
    $arFieldsSection[] = $ob->GetFields();  
}
$arResult["SECTION_NAME"] = $arFieldsSection;
