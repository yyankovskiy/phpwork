<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<?foreach ($arResult["ITEMS"] as $elem)
	$arResult["ELEMS"][$elem["IBLOCK_SECTION_ID"]][] = $elem;?>

<div class="double_tabs">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<div class="blc_hor_tabs" id="submenu-support">
					<ul class="hor_tabs">
						<li class="active open" data-target="all-services">
							<h5 class="vs">Все оказываемые<br>услуги</h5>
							<h5 class="hd">Все оказываемые услуги</h5>
						</li>
						<?foreach ($arResult["SECTION_NAME"] as $arSection):?>
							<li class="open" data-target="<?=$arSection['CODE']?>">
								<?if(!empty($arSection["DESCRIPTION"])){?>
									<h5 class="vs"><?=$arSection["DESCRIPTION"]?></h5>
									<h5 class="hd"><?=$arSection["NAME"]?></h5>
								<?}else{?>
									<h5><?=$arSection["NAME"]?></h5>
								<?}?>
							</li>
						<?endforeach;?>
					</ul>
				</div>
				<ul class="ver_tabs">
					<?foreach ($arResult["SECTION_NAME"] as $key_s => $arSection):?>
                                        <li class="active open" id="<?=$arSection['CODE']?>">
							<div class="direction active"></div>
							<h5><?=$arSection['NAME']?></h5>
							<div class="desc">
								<?foreach ($arResult["ELEMS"][$arSection["ID"]] as $key_e => $elem): ?>
									<div class="desc_txt">
										<span><?=($key_s+1)?>.<?=($key_e+1)?>.</span><p> <?=$elem["NAME"]?></p>
									</div>
								<?endforeach;?>
							</div>
						</li>
					<?endforeach;?>
				</ul>
			</div>
		</div>
	</div><!--container-->	
</div>