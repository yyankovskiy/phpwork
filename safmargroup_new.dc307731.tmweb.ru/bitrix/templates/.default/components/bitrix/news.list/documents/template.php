<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<ul class="charter">
    <?foreach($arResult["ITEMS"] as $elem){?>
    	<li>
			<div class="desc">
				<?if(!empty($elem["PROPERTIES"]["LINK_DOC"]["VALUE"])){
					$file = CFile::GetPath($elem["PROPERTIES"]["LINK_DOC"]["VALUE"]);
				}else if(!empty($elem["PROPERTIES"]["LINK_FORM_DOC"]["VALUE"])){
					$file = $elem["PROPERTIES"]["LINK_FORM_DOC"]["VALUE"];
				}else{
					$file = "#";
				}?>
				<a href="<?=$file?>" target="_blank" class="vs"><?=$elem["PREVIEW_TEXT"]?></a>
				<a href="<?=$file?>" target="_blank" class="hd"><?=$elem["NAME"]?></a>
			</div>
		</li>
    <?}?>
</ul>
