        <?$APPLICATION->IncludeComponent( "bitrix:main.include", ".default", array(
            "AREA_FILE_SHOW" => "file",
            "AREA_FILE_SUFFIX" => "inc",
            "AREA_FILE_RECURSIVE" => "Y",
            "EDIT_TEMPLATE" => "standard.php",
            "PATH" => "/include/footer_contacts.php"
        ), false );?>
    </div><!--wrapper-->

    <div id="footer">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="blc_safmar"><a href="/"><img src="/upload/img/logo.png" alt=""></a></div>
                </div>
            </div>  
            <div class="footer_logos">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <ul class="logos">
                            <li><a href="#"><span class="bl blg1"></span></a></li>
                            <li><a href="#"><span class="bl blg2"></span></a></li>
                            <li><a href="#"><span class="bl blg3"></span></a></li>
                            <li><a href="#"><span class="bl blg4"></span></a></li>
                            <li><a href="#"><span class="bl blg5"></span></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="bg_ftr"></div>                  
    </div><!--footer-->
    <script src="/js/jquery.scroolly.js"></script> 
    <script src="/js/classie.js"></script>	  
    <script>
        jQuery(document).ready(function() {
            $('.blc_bg_menu').scroolly([
                {
                    to: 'con-top',
                    css: {
                        position: 'absolute',
                        top: ''
                    }
                },
                {
                    from: 'con-top',
                    css: {
                        position: 'fixed',
                        top: '0'
                    }
                }
            ], $('.offset'));  
        }); 
			setTimeout(function(){
				(function() {

					// trim polyfill : https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/Trim
					if (!String.prototype.trim) {
						(function() {
							// Make sure we trim BOM and NBSP
							var rtrim = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g;
							String.prototype.trim = function() {
								return this.replace(rtrim, '');
							};
						})();
					}

					[].slice.call( document.querySelectorAll( 'input.input__field' ) ).forEach( function( inputEl ) {
						// in case the input is already filled..
						if( inputEl.value.trim() !== '' ) {
							classie.add( inputEl.parentNode, 'input--filled' );
						}

						// events:
						inputEl.addEventListener( 'focus', onInputFocus );
						inputEl.addEventListener( 'blur', onInputBlur );

					} );

					function onInputFocus( ev ) {
						classie.add( ev.target.parentNode, 'input--filled' );
					}

					function onInputBlur( ev ) {
						if( ev.target.value.trim() === '' ) {
							classie.remove( ev.target.parentNode, 'input--filled' );
						}
					}
				})();			
			}, 200);
	</script>	
	<script src="/js/form/jquery.knob.js"></script>
	<!-- jQuery File Upload Dependencies -->
	<script src="/js/form/jquery.ui.widget.js"></script>
	<script src="/js/form/jquery.iframe-transport.js"></script>
	<script src="/js/form/jquery.fileupload.js"></script>
	<!-- Our main JS file -->
	<script src="/js/form/script.js"></script>