<div class="bg_gray">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <ul class="cnt">
                    <li>
                        <a href="/contact/#map10">
                            <div class="desc">
                                <p class="green">Адрес</p>
                                <?/*<p class="vs">109240, Россия, г. Москва,<br>ул. Верхняя Радищевская,<br>дом 16, строение 2</p>
                                <p class="hd">109240, Россия, г. Москва, ул. Верхняя Радищевская, дом 16, строение 2</p>*/?>
                                <p class="vs">109240, Россия, г. Москва,<br>ул. Верхняя Радищевская,<br>д. 16,стр. 2</p>
                                <p class="hd">109240, Россия, г. Москва, ул. Верхняя Радищевская, д. 16,стр. 2</p>
                                
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="/contact/">
                            <div class="desc">
                                <p class="green">8 800 200 00 00</p>
                                <p class="vs">с 9.00 до 18.00 по<br>московскому времени</p>
                                <p class="hd">с 9.00 до 18.00 по московскому времени</p>
                            </div>
                        </a>    
                    </li>
                    <li>
                        <a href="#">
                            <div class="desc">
                                <p class="green">Обратная связь</p>
                                
                                <p class="vs">Получить данные<br>по своему пенсионному<br>счету </p>
                                <p class="hd">Получить данные по своему пенсионному счету </p>
                            </div>
                        </a>        
                    </li>
                    <li>
                        <a href="mailto:cs@safmarpensions.ru">
                            <div class="desc">
                                <p class="green">Написать нам</p>
                                <p>cs@safmarpensions.ru</p>
                            </div>  
                        </a>    
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>