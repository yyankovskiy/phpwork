<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="theme-color" content="#00c969">

<link rel="shortcut icon" href="/upload/img/favicon.ico">

<!--<link rel="stylesheet" href="/css/scroller.css">-->
<link rel="stylesheet" href="/css/scroolly.demo.min.css">
<link rel="stylesheet" href="/css/bootstrap.min.css">
<link rel="stylesheet" href="/css/fancybox/jquery.fancybox.css">
<link rel="stylesheet" href="/css/chosen/chosen.css">
<link rel="stylesheet" href="/css/feedback.css">
<link rel="stylesheet" href="/css/style.css">

<script type="text/javascript" src="/js/jquery.js"></script>
<script type="text/javascript" src="/js/jquery-ui.js"></script>
<script type="text/javascript" src="/js/datepicker_ru.js"></script>
<script type="text/javascript" src="/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/js/chosen.jquery.js"></script>
<script type="text/javascript" src="/js/jquery.fancybox.pack.js"></script>
<script type="text/javascript" src="/js/jquery.scrollTo.min.js"></script>
<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js"></script>
<script type="text/javascript" src="/js/map_contact.js"></script>
<!--<script type="text/javascript" src="/js/scroller.js"></script>-->
<script type="text/javascript" src="/js/jquery.maskedinput.js"></script>
<script type="text/javascript" src="/js/jquery.validate.js"></script>
<script type="text/javascript" src="/js/jquery.nicescroll.min.js"></script>
<script type="text/javascript" src="/js/action.js"></script>
<script type="text/javascript" src="/js/circles.min.js"></script>