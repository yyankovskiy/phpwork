<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("h1", "Важный аргумент для сотрудничества");
$APPLICATION->SetTitle("Сафмар");
?>

<!--		<div id="wrapper">-->
			
			<div class="blc_pn_top">
				<div class="container">
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<ul class="blc_pn_top_list">
								<li>
									<h2 class="vs">Заявление на<br>получение пенсии</h2>
									<h2 class="hd">Заявление на<br>получение пенсии</h2>									
								</li>
								<li>
									<p>Оформить заявление на получение своей пенсии вы можете одним из 3-х простых способов.</p>
									<p>Выберете наиболее удобный для себя.</p>
								</li>
							</ul>

						</div>	
					</div>			
				</div><!--container-->
			</div><!--blc_pn_top-->
			
			<div class="blc_ftabs">
				<div class="container">
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<ul class="ftabs">
								<li class="active">
									<p class="big_int"><span>1.</span></p>
									<p class="vs">Заполнив и отправив<br>форму</p>
									<p class="hd">Заполнив и отправив форму</p>
								</li>
								<li>
									<p class="big_int"><span>2.</span></p>
									<p class="vs">Скачав и заполнив пакет<br>документов</p>
									<p class="hd">Скачав и заполнив пакет документов</p>
								</li>
								<li>
									<p class="big_int"><span>3.</span></p>
									<p class="vs">Позвонив по телефону<br><a href="tel:88002000000">8 800 200 00 00</a></p>
									<p class="hd">Позвонив по телефону <a href="tel:88002000000">8 800 200 00 00</a></p>
								</li>
							</ul>

						</div>	
					</div>	
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<div class="blc_ftabs_cnt tb1 active">
								<ul class="blc_big_form">
									<li class="vs">
										<div class="desc">
												<h4 class="cnt_fond">Фонд-партнер</h4>
												<h4 class="cnt_data">Персональные данные</h4>
												<h4 class="cnt_info step2" style="display: none">Контактная информация</h4>
												<h4 class="cnt_sms step3" style="display: none">Сообщение</h4>
										</div>								
									</li>
									<li>
										<form action="" method="post"  id="upload">
											<p>Обратиться в пенсионный фонд</p>
											<select data-placeholder="Выберите фонд" name="" id="fond" >
												<!--<option value="Выберите фонд">Выберите фонд</option>-->
												<option value></option>
												<option value="АО НПФ Доверие">АО НПФ Доверие</option>
												<option value="АО НПФ Образование и Наука">АО НПФ Образование и Наука</option>
												<option value="АО НПФ Европейский пенсионный фонд">АО НПФ Европейский пенсионный фонд</option>
												<option value="АО НПФ РЕГИОНФОНД">АО НПФ РЕГИОНФОНД</option>
												<option value="АО НПФ САФМАР">АО НПФ САФМАР</option>
											</select>

											<div class="row ">
												<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
													<p>ФИО</p>
													<ul class="intu">
														<li>
															<span class="input input--juro">
																<input class="input__field input__field--juro star" type="text" id="input-1" autocomplete="off" data-required="step1"/>
																<label class="input__label input__label--juro" for="input-1">
																	<span class="input__label-content input__label-content--juro">Фамилия</span>
																</label>
															</span>															
														</li>
														<li>
															<span class="input input--juro">
																<input class="input__field input__field--juro star" type="text" id="input-2" autocomplete="off" data-required="step1"/>
																<label class="input__label input__label--juro" for="input-2">
																	<span class="input__label-content input__label-content--juro">Имя</span>
																</label>
															</span>															
														</li>
														<li>
															<span class="input input--juro">
																<input class="input__field input__field--juro" type="text" id="input-3" autocomplete="off"/>
																<label class="input__label input__label--juro" for="input-3">
																	<span class="input__label-content input__label-content--juro">Отчество</span>
																</label>
															</span>															
														</li>
													</ul>
												</div>	
											</div>
											
											<div class="row ">
												<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
													<p>Паспорт</p>
													<ul class="intu">
														<li>
															<span class="input input--juro">
																<input class="input__field input__field--juro star" type="text" id="input-4" autocomplete="off" data-required="step1"/>
																<label class="input__label input__label--juro" for="input-4">
																	<span class="input__label-content input__label-content--juro">Номер</span>
																</label>
															</span>															
														</li>
														<li>
															<span class="input input--juro">
																<input class="input__field input__field--juro star input-number datepicker" type="text" id="input-5" autocomplete="off" data-required="step1"/>
																<span class="arv"></span>
																<label class="input__label input__label--juro labstar" for="input-5">
																	<span class="input__label-content input__label-content--juro">Дата выдачи</span>
																</label>
															</span>															
														</li>
														<li>
															<span class="input input--juro">
																<input class="input__field input__field--juro star" type="text" id="input-6" autocomplete="off" data-required="step1"/>
																<label class="input__label input__label--juro" for="input-6">
																	<span class="input__label-content input__label-content--juro">Код подразделения</span>
																</label>
															</span>															
														</li>
													</ul>
												</div>	
											</div>	
											
											<div class="row">
												<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
													<p>СНИЛС</p>
													<ul class="intu one_li">
														<li>
															<span class="input input--juro">
																<input class="input__field input__field--juro star" type="text" id="input-7" data-required="step1"/>
																<label class="input__label input__label--juro" for="input-7">
																	<span class="input__label-content input__label-content--juro">Номер</span>
																</label>
															</span>															
														</li>
													</ul>
												</div>	
											</div>
											
											<div class="step2">
												<div class="row">
													<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
														<p>Средство связи</p>
														<ul class="intu two_li">
															<li>
																<span class="input input--juro">
																	<input class="input__field input__field--juro star phone" type="text" id="input-8" data-required="step2"/>
																	<label class="input__label input__label--juro" for="input-8">
																		<span class="input__label-content input__label-content--juro">Телефон</span>
																	</label>
																</span>															
															</li>
															<li>
																<span class="input input--juro">
																	<input class="input__field input__field--juro star" type="text" id="input-9" data-required="step2"/>
																	<label class="input__label input__label--juro" for="input-9">
																		<span class="input__label-content input__label-content--juro">Email</span>
																	</label>
																</span>															
															</li>														
														</ul>
													</div>	
												</div>	

												<div class="row">
													<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
														<p>Адрес</p>
														<ul class="intu two_li">
															<li>
																<span class="input input--juro">
																	<input class="input__field input__field--juro star" type="text" id="input-10" data-required="step2"/>
																	<label class="input__label input__label--juro" for="input-10">
																		<span class="input__label-content input__label-content--juro">Индекс</span>
																	</label>
																</span>															
															</li>
															<li>
																<select name=""  data-placeholder="Город" id="input-11" >																	
																	<option value="Москва"></option>
																	<option value="Москва">Москва</option>
																	<option value="Санкт-Петербург">Санкт-Петербург</option>
																	<option value="Рязань">Рязань</option>
																	<option value="Астрахань">Астрахань</option>
																	<option value="Псков">Псков</option>
																	<option value="Волгоград">Волгоград</option>
																	<option value="Белгород">Белгород</option>
																	<option value="Владивосток">Владивосток</option>
																	<option value="Хабаровск">Хабаровск</option>
																	<option value="Курск">Курск</option>
																	<option value="Адлер">Адлер</option>
																	<option value="Брянск">Брянск</option>
																	<option value="Валдай">Валдай</option>
																	<option value="Воронеж">Воронеж</option>
																	<option value="Екатеринбург">Екатеринбург</option>
																	<option value="Тверь">Тверь</option>
																	<option value="Ижевск">Ижевск</option>
																	<option value="Суздаль">Суздаль</option>
																	<option value="Тамбов">Тамбов</option>
																	<option value="Иваново">Иваново</option>
																</select>
<!--
																<span class="input input--juro">
																	<input class="input__field input__field--juro star" type="text" id="input-11"/>
																	<label class="input__label input__label--juro" for="input-11">
																		<span class="input__label-content input__label-content--juro">Город</span>
																	</label>
																</span>															
-->
															</li>														
														</ul>
													</div>	
												</div>

												<div class="row">
													<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
														<ul class="intu two_li">
															<li>
																<span class="input input--juro">
																	<input class="input__field input__field--juro star" type="text" id="input-12" data-required="step2"/>
																	<label class="input__label input__label--juro" for="input-12">
																		<span class="input__label-content input__label-content--juro">Улица </span>
																	</label>
																</span>															
															</li>
															<li>
																<ul class="in_two">
																	<li>
																		<span class="input input--juro">
																			<input class="input__field input__field--juro star input-number number" type="text" id="input-13" data-required="step2"/>
																			<label class="input__label input__label--juro labstar" for="input-13">
																				<span class="input__label-content input__label-content--juro">Дом</span>
																			</label>
																		</span>																		
																	</li>
																	<li>
																		<span class="input input--juro">
																			<input class="input__field input__field--juro star input-number number" type="text" id="input-14" data-required="step2"/>
																			<label class="input__label input__label--juro labstar" for="input-14">
																				<span class="input__label-content input__label-content--juro">Кварт.</span>
																			</label>
																		</span>																		
																	</li>
																</ul>
															</li>														
														</ul>
													</div>	
												</div>

												<div class="row">
													<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

														<ul class="intu two_li">
															<li>
																<p>Способ получения ответа</p>
																<ul class="in_two rdb">
																	<li>	
																		<input type="radio" name="radiog_lite" id="radio1" class="css-checkbox" checked="checked"/>
																		<label for="radio1" class="css-label radGroup1"><span>E-mail</span></label>
																	</li>
																	<li>
																		<input type="radio" name="radiog_lite" id="radio2" class="css-checkbox" />
																		<label for="radio2" class="css-label radGroup1"><span>почтой России</span></label>
																	</li>
																</ul>
															</li>
															<li>
																<p>Адрес дос-ки совпадает с указанным</p>
																<ul class="in_two rdb">
																	<li>	
																		<input type="radio" name="radiog_lite2" id="radio3" class="css-checkbox" checked="checked"/>
																		<label for="radio3" class="css-label radGroup1"><span>Да</span></label>
																	</li>
																	<li>
																		<input type="radio" name="radiog_lite2" id="radio4" class="css-checkbox" />
																		<label for="radio4" class="css-label radGroup1"><span>Нет</span></label>
																	</li>
																</ul>
															</li>														
														</ul>
													</div>	
												</div>	

												
											</div>
											<div class="step2_2">
													<div class="row">
														<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
															<p>Адрес доставки</p>
															<ul class="intu two_li">
																<li>
																	<span class="input input--juro">
																		<input class="input__field input__field--juro star" type="text" id="input-15"/>
																		<label class="input__label input__label--juro" for="input-15">
																			<span class="input__label-content input__label-content--juro">Индекс</span>
																		</label>
																	</span>															
																</li>
																<li>
																	<select name="" data-placeholder="Город" id="input-16">
																		<option value="Москва"></option>
																		<option value="Москва">Москва</option>
																		<option value="Санкт-Петербург">Санкт-Петербург</option>
																		<option value="Рязань">Рязань</option>
																		<option value="Астрахань">Астрахань</option>
																		<option value="Псков">Псков</option>
																		<option value="Волгоград">Волгоград</option>
																		<option value="Белгород">Белгород</option>
																		<option value="Владивосток">Владивосток</option>
																		<option value="Хабаровск">Хабаровск</option>
																		<option value="Курск">Курск</option>
																		<option value="Адлер">Адлер</option>
																		<option value="Брянск">Брянск</option>
																		<option value="Валдай">Валдай</option>
																		<option value="Воронеж">Воронеж</option>
																		<option value="Екатеринбург">Екатеринбург</option>
																		<option value="Тверь">Тверь</option>
																		<option value="Ижевск">Ижевск</option>
																		<option value="Суздаль">Суздаль</option>
																		<option value="Тамбов">Тамбов</option>
																		<option value="Иваново">Иваново</option>
																	</select>																
	<!--
																	<span class="input input--juro">
																		<input class="input__field input__field--juro star" type="text" id="input-16"/>
																		<label class="input__label input__label--juro" for="input-16">
																			<span class="input__label-content input__label-content--juro">Город</span>
																		</label>
																	</span>															
	-->
																</li>														
															</ul>
														</div>	
													</div>

													<div class="row">
														<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
															<ul class="intu two_li">
																<li>
																	<span class="input input--juro">
																		<input class="input__field input__field--juro star" type="text" id="input-17"/>
																		<label class="input__label input__label--juro" for="input-17">
																			<span class="input__label-content input__label-content--juro">Улица </span>
																		</label>
																	</span>															
																</li>
																<li>
																	<ul class="in_two">
																		<li>
																			<span class="input input--juro">
																				<input class="input__field input__field--juro star input-number number" type="text" id="input-18"/>
																				<label class="input__label input__label--juro labstar" for="input-18">
																					<span class="input__label-content input__label-content--juro">Дом</span>
																				</label>
																			</span>																		
																		</li>
																		<li>
																			<span class="input input--juro">
																				<input class="input__field input__field--juro star input-number number" type="text" id="input-19"/>
																				<label class="input__label input__label--juro labstar" for="input-19">
																					<span class="input__label-content input__label-content--juro">Кварт.</span>
																				</label>
																			</span>																		
																		</li>
																	</ul>
																</li>														
															</ul>
														</div>	
													</div>
												</div>
											
											<div class="step3">
												<div class="row">
													<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
														<p>Ваше обращение в фонд</p>
														<ul class="intu one_li">
															<li>
																<div class="txtr"><textarea name="" id="" placeholder="Текст обращения"></textarea></div>
															</li>														
														</ul>
													</div>	
												</div>	
											</div>
											
											<div class="row">
												<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
													<div class="blc_fole_upl">
														<div id="drop">
															<a class="dwn">Загрузить файл</a>
															<p class="desc">Размер файла не должен превышать 3 Мгб. Формат: txt, doc, docx, pdf, gif, bmp, jpg, jpeg, png, tif, tiff, zip, 7z, rar</p>														
															<input type="file" name="upl" multiple />
														</div>
														<ul>
															<!-- The file uploads will be shown here -->
														</ul>
													</div>
												</div>												
											</div>	
											
											<div class="row">
												<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
													<div class="blc_iagree">
														<ul class="">
															<li>	
																<input type="checkbox" name="checkboxG100" id="checkboxG100" class="css-checkbox" />
																<label for="checkboxG100" class="css-label">Я согласен на обработку моих персональных данных.
																	<a href="#">Подробнее</a>
																</label>
															</li>
															<li>
																<input type="checkbox" name="checkboxG200" id="checkboxG200" class="css-checkbox" checked="checked"/>																	<label for="checkboxG200" class="css-label">Я согласен на предоставление мне информации и предложение продуктов и услуг Фонда и Пенсионного Администратора путем направления почтовой корреспонденции по моему домашнему адресу, посредством электронной почты, телефонных обращений, SMS – сообщений.</label>
															</li>
														</ul>
													</div>
												</div>												
											</div>
											
											<div class="row">
												<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
													<div class="blc_btn">
														<button>Отправить</button>
													</div>
												</div>												
											</div>
											
											<div class="row">
												<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
													<div class="upl_files">
														<ul>
															<li>
																<div class="desc">
																	<a href="" target="_blank" class="vs">Порядок приема<br>и рассмотрения обращения</a>
																</div>
															</li>																					
														</ul>														
													</div>
												</div>												
											</div>
											
																																																																																													<div class="row">
												<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
													<p>ERROR</p>
													<ul class="intu one_li">
														<li>
															<span class="input input--juro error">
																<input class="input__field input__field--juro star error" type="text" id="input-70"/>
																<label class="input__label input__label--juro" for="input-70">
																	<span class="input__label-content input__label-content--juro">Ошибка</span>
																</label>
															</span>															
														</li>
													</ul>
												</div>	
											</div>																																																																																			
										</form>
									</li>
								</ul>
							</div>	
							<div class="blc_ftabs_cnt tb2">
								<div class="row">
									<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
										<ul class="list_tb2">
											<li>
												<p>Скачайте весь пакет документов для подачи заявки в удобной для вас форме: 
												   или для печати, или для заполнения в microsoft exel. И отправьте нам эти документы по почте.</p>
												<ul class="list_tb2_in_folder">
													<li>
														<div class="desc">
															<a href="" target="_blank" class="vs">Пакет документов для<br>печати (в формате PDF)</a>   
														</div>												  		
													</li>
													<li>
														<div class="desc">
															<a href="" target="_blank" class="vs">Пакет документов для<br>заполнения (в формате EXL)</a>   
														</div>												  		
													</li>
												</ul>
											</li>
											<li>
												<p>Если Вы не имеете возможности направить запрос в соответствии с предъявленными требованиями, 
												   обратиться в Пенсионный администратор или фонды  можно следующими способами:</p>
												<ul class="list_tb2_in_folder">
													<li>
														По электронной почте <a href="mailto:customer_service@safmarpensions.ru">customer_service@safmarpensions.ru</a>
													</li>
													<li>
														По почте, направив письмо на адрес 109240, г. Москва, ул. Верхняя Радищевская, д.16, стр. 2-3	
													</li>
													<li>
														Привезти письмо лично и сдать его можно по вышеуказанному адресу. 
														Прием документов с 9.00 до 18.00 по рабочим дням. При себе лучше иметь копию письма – 
														Вам поставят отметку о его приеме. Для прохода в здание Вам необходимо иметь документ, удостоверяющий личность.
													</li>
													<li>
														Обратиться по телефону <a href="tel:88003030777">8 800 30-30-777</a>	
													</li>																									
												</ul>											
											</li>
										</ul>
									</div>	
								</div>	
								<div class="row">
									<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
										<div class="upl_files">
											<ul>
												<li>
													<div class="desc">
														<a href="" target="_blank" class="vs">Порядок приема<br>и рассмотрения обращения</a>
													</div>
												</li>																					
											</ul>														
										</div>
									</div>												
								</div>													
							</div>
							<div class="blc_ftabs_cnt tb3">
								<div class="row">
									<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
										<ul class="list_tb2">
											<li>
												<p>Позвоните нам по телефону и наши специалисты запонят все необходимые заявки за вас и проконсультируют о документах, которые вам необходимо предоставить.</p>
											</li>
											<li>
												<p>Телефон нашего колл-центра:</p>
												<p><a href="tel:88002000000">8 800 200 00 00</a></p>										
											</li>
										</ul>
									</div>	
								</div>	
								<div class="row">
									<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
										<div class="upl_files">
											<ul>
												<li>
													<div class="desc">
														<a href="" target="_blank" class="vs">Порядок приема<br>и рассмотрения обращения</a>
													</div>
												</li>																					
											</ul>														
										</div>
									</div>												
								</div>
							</div>
						</div>	
					</div>																	
				</div><!--container-->
			</div><!--blc_ftabs-->			
			
<!--		</div>-->
		<!--wrapper-->


 <? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>