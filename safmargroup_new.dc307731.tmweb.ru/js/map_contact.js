jQuery(document).ready(function() {
        var map1;
        jQuery(function($) {
            setTimeout(function() {initialize(map1, 'map10'),500});;
        });

        function initialize(varName, mapName) {

                //var map;   
                var my_map = 'custom_style';
                //var bounds = new google.maps.LatLngBounds();
                var featureOpts = [{"featureType":"administrative","elementType":"labels.text.fill","stylers":[{"color":"#444444"}]},{"featureType":"landscape","elementType":"all","stylers":[{"color":"#f2f2f2"}]},{"featureType":"poi","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"poi","elementType":"labels.text","stylers":[{"visibility":"off"}]},{"featureType":"road","elementType":"all","stylers":[{"saturation":-100},{"lightness":45}]},{"featureType":"road.highway","elementType":"all","stylers":[{"visibility":"simplified"}]},{"featureType":"road.arterial","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"water","elementType":"all","stylers":[{"color":"#dbdbdb"},{"visibility":"on"}]}]

                $('body').find('#' + mapName).each(function(){
                        var Moscow = new google.maps.LatLng($(this).parent().find('span:first').attr('y'), $(this).parent().find('span:first').attr('x'));
                        var mapOptions = {
                                center: Moscow,
                                panControl: true,
                                scaleControl:true,
                                streetViewControl: true,
                                overviewMapControl: true,
                                rotateControl:true,
                                zoom: 14,
                                zoomControl: true,
                                zoomControlOptions: {
                                  style:google.maps.ZoomControlStyle.DEFAULT
                                },                                                             
                                mapTypeControl: false,
                                scrollwheel: false,
                                mapTypeControlOptions: {
                                  mapTypeIds: [google.maps.MapTypeId.ROADMAP, my_map]
                                },
                                mapTypeId: my_map
                        }; // end mapOptions;

                        varName = new google.maps.Map(document.getElementById($(this).attr('id')), mapOptions);
  
                       //varName.panBy(100,0);// maps shift center

                        if(mapName == 'map10'){
                            map1 = varName;
                        }
                        var ob = [];
                        var i = 0;
                        $(this).parent().find('span').each(function(index, elem){
                                ob[i] = {
                                        ele: 'Moscow '+ i,
                                        name: $(this).attr('name'),
                                        text: $(this).attr('text'),
                                        url: $(this).attr('url'),
                                        x: $(this).attr('x'),
                                        y: $(this).attr('y')
                                        };
                                i = i+1;
                        });
                        var count_element = i;
                        varName.setTilt(45);
                        var markers = [];
                        // Display multiple markers on a map
                        var infoWindow = new google.maps.InfoWindow(), marker, i;
                        // Loop through our array of markers & place each one on the map  
                        for( i = 0; i < markers.length; i++ ) {
                               // var position = new google.maps.LatLng(markers[i][1], markers[i][2]);

                                //bounds.extend(position);
                                      // marker = new google.maps.Marker({
                                      //         position: position,
                                      //         map: map,
                                      //         title: markers[i][0]
                                      // });
                                // Allow each marker to have an info window    
                                // google.maps.event.addListener(marker, 'click', (function(marker, i) {
                                //     return function() {
                                //         infoWindow.setContent(infoWindowContent[i][0]);
                                //         infoWindow.open(varName, marker);
                                //         }
                                // })(marker, i)); 
                        } 
                        var styledMapOptions = {name: 'Custom Style'};
                        var customMapType = new google.maps.StyledMapType(featureOpts, styledMapOptions);

                        varName.mapTypes.set(my_map, customMapType);
                        var j = 0;

                        while(j<count_element){
                                var officeImage = new google.maps.MarkerImage('/upload/img/marker_map.png',
                                        new google.maps.Size(28,38),
                                        //new google.maps.Point(0,0),
                                        new google.maps.Point(0, 0)
                                );

                                var officeShadow = new google.maps.MarkerImage('/upload/img/marker_map.png',
                                        new google.maps.Size(28,38),
                                        //new google.maps.Point(0,0),
                                        new google.maps.Point(0, 0)
                                );

                                var office = [
                                        [ob[j]['ele'], ob[j]['y'], ob[j]['x']]
                                ];

                                var infoWindowContentsOffice = [
                                        ['<div class="info_content">' + '<h3><a href="'+ob[j]['url']+'">'+ob[j]['name']+'</a></h3>' + '<p>'+ob[j]['text']+'</p>' + '</div>']
                                ];

                                var infoOffice = new google.maps.InfoWindow(), office, i;
                                var position = new google.maps.LatLng(office[i][1], office[i][2]);

                                office = new google.maps.Marker({
                                        position: position,
                                        map: varName,
                                        icon: officeImage,
                                        shadow: officeShadow,
                                        optimized: false,
                                        zIndex:9,
                                        title: office[i][0]
                                });

                                google.maps.event.addListener(office, 'click', (function(office, i) {
                                        return function() {
                                                infoOffice.setContent(infoWindowContentsOffice[i][0]);
                                                infoOffice.open(varName, office);
                                        };
                                })(office, i));
                                j = j+1;
                        };
                });
        } // end initialize()
});