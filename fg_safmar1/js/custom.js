jQuery(document).ready(function () {
	
 	/*$('.hor_tabs li:first-child').click(function(){	
		if($(this).is('.active_plus')){
			$('.hor_tabs li:first-child').removeClass('active_plus');
			$('.ver_tabs li .desc').slideDown();
			$('.hor_tabs li').removeClass('active');
			$('.hor_tabs li:first-child').addClass('active');
			$('.ver_tabs li .direction ').addClass('active');
			$('.hor_tabs li').addClass('open');
			$('.ver_tabs li').addClass('open');
		}else{
			$(this).toggleClass('active');
			$('.ver_tabs li').addClass('active open');
			$('.hor_tabs li').addClass('open');
			$('.ver_tabs li .desc').slideToggle();
			$('.ver_tabs li .direction').toggleClass('active');			
		}
	});
	$('.hor_tabs li:nth-child(2)').click(function(){
		if($('.hor_tabs li').is('.open')){
			$('.hor_tabs li, .ver_tabs li').removeClass('open');
			$('.ver_tabs li .desc').slideUp();
			$('.hor_tabs li:first-child').removeClass('active');
			$('.hor_tabs li:nth-child(2)').addClass('active');
			$('.ver_tabs li:first-child .desc').slideDown();
			$('.ver_tabs li .direction ').removeClass('active');
			$('.ver_tabs li:first-child .direction ').addClass('active');
			$('.hor_tabs li:first-child').addClass('active_plus');
		}else{
			$('.hor_tabs li:first-child').addClass('active_plus');
			$('.hor_tabs li, .ver_tabs li').removeClass('open');			
			$(this).toggleClass('active');	
			$('.ver_tabs li:first-child .desc').slideToggle();
			$('.ver_tabs li:first-child').toggleClass('active');
			$('.ver_tabs li:first-child .direction').toggleClass('active');			
		}
	});
 	$('.hor_tabs li:nth-child(3)').click(function(){
		if($('.hor_tabs li').is('.open')){
			$('.hor_tabs li, .ver_tabs li').removeClass('open');
			$('.ver_tabs li .desc').slideUp();
			$('.hor_tabs li:first-child').removeClass('active');
			$('.hor_tabs li:nth-child(3)').addClass('active');
			$('.ver_tabs li:nth-child(2) .desc').slideDown();
			$('.ver_tabs li .direction ').removeClass('active');
			$('.ver_tabs li:nth-child(2) .direction ').addClass('active');
			$('.hor_tabs li:first-child').addClass('active_plus');
		}else{
			$('.hor_tabs li:first-child').addClass('active_plus');
			$('.hor_tabs li, .ver_tabs li').removeClass('open');			
			$(this).toggleClass('active');
			$('.ver_tabs li:nth-child(2) .desc').slideToggle();
			$('.ver_tabs li:nth-child(2)').toggleClass('active');
			$('.ver_tabs li:nth-child(2) .direction').toggleClass('active');
		}
	});
 	$('.hor_tabs li:nth-child(4)').click(function(){		
		if($('.hor_tabs li').is('.open')){
			$('.hor_tabs li, .ver_tabs li').removeClass('open');
			$('.ver_tabs li .desc').slideUp();
			$('.hor_tabs li:first-child').removeClass('active');
			$('.hor_tabs li:nth-child(4)').addClass('active');
			$('.ver_tabs li:nth-child(3) .desc').slideDown();
			$('.ver_tabs li .direction ').removeClass('active');
			$('.ver_tabs li:nth-child(3) .direction ').addClass('active');
			$('.hor_tabs li:first-child').addClass('active_plus');
		}else{	
			$('.hor_tabs li:first-child').addClass('active_plus');
			$('.hor_tabs li, .ver_tabs li').removeClass('open');			
			$(this).toggleClass('active');
			$('.ver_tabs li:nth-child(3) .desc').slideToggle();
			$('.ver_tabs li:nth-child(3)').toggleClass('active');
			$('.ver_tabs li:nth-child(3) .direction').toggleClass('active');
		}
	});
 	$('.hor_tabs li:nth-child(5)').click(function(){
		if($('.hor_tabs li').is('.open')){
			$('.hor_tabs li, .ver_tabs li').removeClass('open');
			$('.ver_tabs li .desc').slideUp();
			$('.hor_tabs li:first-child').removeClass('active');
			$('.hor_tabs li:nth-child(5)').addClass('active');
			$('.ver_tabs li:nth-child(4) .desc').slideDown();
			$('.ver_tabs li .direction ').removeClass('active');
			$('.ver_tabs li:nth-child(4) .direction ').addClass('active');
			$('.hor_tabs li:first-child').addClass('active_plus');
		}else{
			$('.hor_tabs li:first-child').addClass('active_plus');
			$('.hor_tabs li, .ver_tabs li').removeClass('open');			
			$(this).toggleClass('active');
			$('.ver_tabs li:nth-child(4) .desc').slideToggle();
			$('.ver_tabs li:nth-child(4)').toggleClass('active');
			$('.ver_tabs li:nth-child(4) .direction').toggleClass('active');
		}
	});	
 	$('.hor_tabs li:last-child').click(function(){		
		if($('.hor_tabs li').is('.open')){
			$('.hor_tabs li, .ver_tabs li').removeClass('open');
			$('.ver_tabs li .desc').slideUp();
			$('.hor_tabs li:first-child').removeClass('active');
			$('.hor_tabs li:last-child').addClass('active');
			$('.ver_tabs li:nth-child(5) .desc').slideDown();
			$('.ver_tabs li .direction ').removeClass('active');
			$('.ver_tabs li:nth-child(5) .direction ').addClass('active');
			$('.hor_tabs li:first-child').addClass('active_plus');
		}else{	
			$('.hor_tabs li:first-child').addClass('active_plus');
			$('.hor_tabs li, .ver_tabs li').removeClass('open');			
			$(this).toggleClass('active');
			$('.ver_tabs li:last-child .desc').slideToggle();
			$('.ver_tabs li:last-child').toggleClass('active');
			$('.ver_tabs li:last-child .direction').toggleClass('active');
		}
	});
 	$('.ver_tabs li:first-child .direction, .ver_tabs li:first-child h5').click(function(){
		if($('.ver_tabs li').is('.open')){
			$('.hor_tabs li, .ver_tabs li').removeClass('open');
			$('.ver_tabs li .desc').slideUp();
			$(this).closest('li').find('.desc').slideDown();
			$('.hor_tabs li:nth-child(2)').addClass('active');
			$('.hor_tabs li:first-child').removeClass('active');
			$('.ver_tabs li .direction').removeClass('active');
			$(this).addClass('active');
			$('.hor_tabs li:first-child').addClass('active_plus');
		}else{
			$('.hor_tabs li:first-child').addClass('active_plus');
			$(this).toggleClass('active');
			$(this).prev('.direction').toggleClass('active');
			$(this).closest('li').toggleClass('active');
			$(this).closest('li').find('.desc').slideToggle();
			$('.hor_tabs li:nth-child(2)').toggleClass('active');			
		}
	});
 	$('.ver_tabs li:nth-child(2) .direction, .ver_tabs li:nth-child(2) h5').click(function(){
		if($('.ver_tabs li').is('.open')){
			$('.hor_tabs li, .ver_tabs li').removeClass('open');
			$('.ver_tabs li .desc').slideUp();
			$(this).closest('li').find('.desc').slideDown();
			$('.hor_tabs li:nth-child(3)').addClass('active');
			$('.hor_tabs li:first-child').removeClass('active');
			$('.ver_tabs li .direction').removeClass('active');
			$(this).addClass('active');	
			$('.hor_tabs li:first-child').addClass('active_plus');
		}else{
			$('.hor_tabs li:first-child').addClass('active_plus');
			$(this).toggleClass('active');
			$(this).prev('.direction').toggleClass('active');
			$(this).closest('li').toggleClass('active');
			$(this).closest('li').find('.desc').slideToggle();
			$('.hor_tabs li:nth-child(3)').toggleClass('active');			
		}		
	});	
 	$('.ver_tabs li:nth-child(3) .direction, .ver_tabs li:nth-child(3) h5').click(function(){
		if($('.ver_tabs li').is('.open')){
			$('.hor_tabs li, .ver_tabs li').removeClass('open');
			$('.ver_tabs li .desc').slideUp();
			$(this).closest('li').find('.desc').slideDown();
			$('.hor_tabs li:nth-child(4)').addClass('active');
			$('.hor_tabs li:first-child').removeClass('active');
			$('.ver_tabs li .direction').removeClass('active');
			$(this).addClass('active');	
			$('.hor_tabs li:first-child').addClass('active_plus');
		}else{
			$('.hor_tabs li:first-child').addClass('active_plus');
			$(this).toggleClass('active');
			$(this).prev('.direction').toggleClass('active');
			$(this).closest('li').toggleClass('active');
			$(this).closest('li').find('.desc').slideToggle();
			$('.hor_tabs li:nth-child(4)').toggleClass('active');			
		}		
	});	
 	$('.ver_tabs li:nth-child(4) .direction, .ver_tabs li:nth-child(4) h5').click(function(){
		if($('.ver_tabs li').is('.open')){
			$('.hor_tabs li, .ver_tabs li').removeClass('open');
			$('.ver_tabs li .desc').slideUp();
			$(this).closest('li').find('.desc').slideDown();
			$('.hor_tabs li:nth-child(5)').addClass('active');
			$('.hor_tabs li:first-child').removeClass('active');
			$('.ver_tabs li .direction').removeClass('active');
			$(this).addClass('active');
			$('.hor_tabs li:first-child').addClass('active_plus');
		}else{
			$('.hor_tabs li:first-child').addClass('active_plus');
			$(this).toggleClass('active');
			$(this).prev('.direction').toggleClass('active');
			$(this).closest('li').toggleClass('active');
			$(this).closest('li').find('.desc').slideToggle();
			$('.hor_tabs li:nth-child(5)').toggleClass('active');			
		}			
	});	
 	$('.ver_tabs li:last-child .direction, .ver_tabs li:last-child h5').click(function(){
		if($('.ver_tabs li').is('.open')){
			$('.hor_tabs li, .ver_tabs li').removeClass('open');
			$('.ver_tabs li .desc').slideUp();
			$(this).closest('li').find('.desc').slideDown();
			$('.hor_tabs li:last-child').addClass('active');
			$('.hor_tabs li:first-child').removeClass('active');
			$('.ver_tabs li .direction').removeClass('active');
			$(this).addClass('active');	
			$('.hor_tabs li:first-child').addClass('active_plus');
		}else{
			$('.hor_tabs li:first-child').addClass('active_plus');
			$(this).toggleClass('active');
			$(this).prev('.direction').toggleClass('active');
			$(this).closest('li').toggleClass('active');
			$(this).closest('li').find('.desc').slideToggle();
			$('.hor_tabs li:last-child').toggleClass('active');			
		}		
	});
	
*/
CheckSubmenuPosition();
//CloseAllContent();
  var topLogo = $('.logo').innerHeight() + 30;

    jQuery(window).scroll(function(){
		if(jQuery(window).scrollTop() >= topLogo){
			jQuery('.logo_fix').addClass('active'); 
		} 
        else {
        	jQuery('.logo_fix').removeClass('active'); 
        }  
		
		CheckSubmenuPosition();
	  
    });

	$('.ver_tabs li').unbind("click").bind("click",function(){		
		if($(this).is('.open')){
			CloseContent($(this));
		}else{
			OpenContent($(this));
		}		
	});
	$("#submenu-support .hor_tabs li").unbind("click").bind("click",function(){
		if($(this).attr('data-target')!="all-services")
		{
			//$("#submenu-support .hor_tabs li").removeClass("active");
			if($(this).hasClass("active"))
			{
				$(this).removeClass("active")
			}else
			{
				$("#submenu-support .hor_tabs li[data-target='all-services']").removeClass("active");
				$(this).addClass("active");
			}
			
			var element=$("#"+$(this).attr('data-target'));
			
			if($(this).hasClass("active"))
			{				
				$("#submenu-support .hor_tabs li:not(.active)").each(function(){
					CloseContent($("#"+$(this).attr('data-target')));
				});
				OpenContent(element);
			}else
			{
				CloseContent(element);
			}
			setTimeout(function(){
			var additional=$('#submenu-support').innerHeight()+$('.blc_bg_menu').innerHeight()+25;
			$.scrollTo(element.offset().top-additional, 500);	
			}, 400);

			
			//$.scrollTo(element.position().top+additional, 500);
			
		}else
		{
			var state=$(this).hasClass("active");
			
			$("#submenu-support .hor_tabs li").removeClass("active");
			
			if(state)
			{
				$(this).removeClass("active")
			}else
			{
				$(this).addClass("active");
			}
			var additional=$('#submenu-support').innerHeight()+$('.blc_bg_menu').innerHeight()+25;
			$.scrollTo($(".ver_tabs").offset().top-additional, 500);
			if($(this).hasClass("active"))
			{
				OpenAllContent();
			}else
			{
				CloseAllContent();
			}
		}
	});
	
	function CloseAllContent()
	{
		$('.ver_tabs li').each(function(){
			CloseContent($(this));
		});
	}
	function OpenAllContent()
	{
		$('.ver_tabs li').each(function(){			
			OpenContent($(this));
			if($(this).hasClass('open')){
				OpenContent($(this));
			}
		});
	}
	
	function OpenContent(element)
	{
		if(!element.find('.direction').hasClass('active')){
			//$('.hor_tabs li:first-child').addClass('active_plus');
			element.toggleClass('active');
			element.find('.direction').addClass('active');
			element.closest('li').toggleClass('active');
			element.closest('li').find('.desc').slideToggle();
			//$('.hor_tabs li:last-child').toggleClass('active');			
		}
	}
	function CloseContent(element)
	{
		if(element.find('.direction').hasClass('active')){
			element.removeClass('open');
			element.find('.desc').slideUp();
			//$(this).closest('li').find('.desc').slideDown();
			//$('.hor_tabs li:last-child').addClass('active');
			//$('.hor_tabs li:first-child').removeClass('active');
			element.find('.direction').removeClass('active');
			//$(this).addClass('active');	
			//$('.hor_tabs li:first-child').addClass('active_plus');
		}
	}
	
	function CheckSubmenuPosition()
	{
		var topPosition=$('#submenu-support').offset().top;
		if($('#submenu-support').attr("data-top")!="0")
		{
			topPosition=$('#submenu-support').attr("data-top");
		}
		var topPositionValue=parseInt(topPosition);
		if($(window).scrollTop()+100>=topPositionValue)
		{
			if($('#submenu-support').attr("data-top")=="0")
			{
				$('#submenu-support').attr("data-top",$('#submenu-support').offset().top);			
				$('#submenu-support').parent("div").css("margin-top","120px");
			}
			$('#submenu-support').addClass("hor_tabs_fix");
			$('#submenu-support').css("margin-top","70px");
		}
		
		else
		{
			if($('#submenu-support').attr("data-top")!="0")
			{
				$('#submenu-support').attr("data-top","0");
				$('#submenu-support').parent("div").css("margin-top","auto");
			}
			$('#submenu-support').removeClass("hor_tabs_fix");
			$('#submenu-support').css("margin-top","auto");
		}
	}
	//$(".fancybox").fancybox();
	
//    $("select").chosen({
//		//"disable_search": true, //поле поиска выключено
//		"disable_search": false, //поле поиска включено
//		width: '100%'
//	});
	

	
	(function($) {
		$(function() {
			$('ul.part').on('click', 'li:not(.active)', function() {
				$(this)
					.addClass('active').siblings().removeClass('active')
					.closest('div.tabs').find('div.tabs__content').removeClass('active').eq($(this).index()).addClass('active');
			});
		});
	})(jQuery);	
	
//	var myWidth = 0;
//	myWidth = window.innerWidth;
//	jQuery('body').prepend('<div id="size" style="background:#000;padding:5px;position:fixed;z-index:99999;color:#fff;">Width = '+myWidth+'</div>');
//	jQuery(window).resize(function(){
//		var myWidth = 0;
//		myWidth = window.innerWidth;
//		jQuery('#size').remove();
//		jQuery('body').prepend('<div id="size" style="background:#000;padding:5px;position:fixed;z-index:99999;color:#fff;">Width = '+myWidth+'</div>');
//	});	
	
	
});



