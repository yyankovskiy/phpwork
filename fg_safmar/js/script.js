$(document).ready(function () {
	var lastPosition = 0;
	var currentPage = 1;
	var totalPage = $('[id^="page"]').length;
	var lockScrolling = false;
	for (var i = 1; i <= totalPage; i++) {
		var leftPosition = 0;//$("#page1 h1").offset().left-$("#page1 h1").width()/2;
		$("#page" + i + " h1").attr("data-first", leftPosition);
	}

	$('.blc_close').on('click', function () {
		var bigShowTop = $('.big_show_top');
		//bigShowTop.css("top", -bigShowTop.height() + "px")
		//				.css("display", "block");
		//.css("height", height + "px");
		var blcBigMenu = $('.big_show_bottom');
		

		//blcBigMenu.stop().animate({ 'height': $(window).height() + "px" }, 1700);
		blcBigMenu.stop().animate({ 'height': $(window).height() + "px" }, 1000, function () {
			BlurElement($('#wrapper'), 0);
			BlurElement($('.bg_main'), 0);
			blcBigMenu.stop().animate({ 'top': $(window).height() + "px" }, 1000, function() {
				bigShowTop.css("display", "none");
			})
			bigShowTop.stop().animate({ 'top': -bigShowTop.height() + "px" }, 500, function () {
				//ShowDefaultMenu();
				//blcBigMenu.css('top', bigShowTop.height() + "px");
			});
			ShowDefaultMenu();
		});

		$('ul.lang li').removeClass('active');
		$('ul.lang li:first-child').addClass('active');
	});

	$('.blc_menu_btn').on('click', function () {
		if ($("#main-footer").hasClass('all_footer')) {
			$('#header').css("height", "0")


			var height = $(window).height();
			var blcBigMenu = $('.big_show_bottom');
			var bigShowTop = $('.big_show_top');


			blcBigMenu.css("top", height + "px")
				.css("display", "block")
				.css("height", height + "px");
			//blcBigMenu.find('.container').css("height", height + "px");
			bigShowTop.css("top", -bigShowTop.height() + "px")
				.css("display", "block");
			HideDefaultMenu(700, function() {
				bigShowTop.stop().animate({ 'top': "0px" }, 500);


				//.css("height", height + "px");
			});
			//blcBigMenu.stop().animate({ 'top': "0px" }, 1700, function () {

			blcBigMenu.stop().animate({ 'top': "0px" }, 1000, function() {
				//var menuHeight = parseInt(blcBigMenu.find('.brdh').css("margin-top").replace("px", "")) + 113;
				var menuHeight = 0;
				var menuContainerHeight = blcBigMenu.find('#menu-container').innerHeight();

				$('#wrapper').stop().animate({ '-webkit-filter': 10 }, { step: function(now, fx) { BlurElement($(this), now); }, duration: 'slow' });
				$('.bg_main').stop().animate({ '-webkit-filter': 10 }, { step: function(now, fx) { BlurElement($(this), now); }, duration: 'slow' });

				blcBigMenu.stop().animate({ 'height': Math.round(menuHeight + menuContainerHeight) + "px" }, 1000)
				//blcBigMenu.find('.big_menu').stop().animate({ 'height': Math.round(menuHeight + menuContainerHeight) + "px" }, 1700)

			});

			$('ul.lang li').removeClass('active');
			$('ul.lang li:first-child').addClass('active');
		}
	});

	$('.bullets li').click(function() {
		var page = parseInt($(this).attr('data-page'));
	//	if (currentPage != page) {
		if (currentPage <= page) {
			if (currentPage < page) {

				for (var j = currentPage; j <= page; j++) {
					currentPage = j;
					scrollDownProcess(currentPage);
					setTimeout(function () {
					}, 600);
				}
			} else {
				currentPage = page;
				scrollDownProcess(currentPage);
			}
				
			} else {
				$("#page" + currentPage).stop().animate({ opacity: 0 }, 1700);
				currentPage = page+1;
				scrollUpProcess(currentPage);
			}
	//	}
		
	});

	$('#nextBtn').click(function() {
		scrollDownProcess(currentPage);
	});

	$('#wrapper').on('DOMMouseScroll mousewheel', function (event) {
		if (!lockScrolling) {
			if (event.originalEvent.detail > 0 || event.originalEvent.wheelDelta < 0) { //alternative options for wheelData: wheelDeltaX & wheelDeltaY
				if ($("#main-footer").hasClass('all_footer') && currentPage > 0) {
					//scroll down
					scrollDownProcess(currentPage);
				}
			} else {
				//scroll up
				if (currentPage > 1) {
					$("#page" + currentPage).stop().animate({ opacity: 0 }, 1700);
					currentPage--;
					scrollUpProcess(currentPage);
				}
				/*if (currentPage < 1) {
					currentPage = 1;
					lockScrolling = false;
				}*/
			}

			//prevent page fom scrolling
			if ($("#main-footer").hasClass('all_footer')) {
				return false;
			}
		}
	});

	function scrollDownProcess(id) {
		lockScrolling = true;
		if ($("#page" + id + " h1").attr("data-hidden") != 'true') {
			$("#page" + id + " h1").stop().animate({ 'margin-left': -Math.round($(window).width() + $("#page" + id + " h1").width()) + "px", opacity: 0 }, 1700, function () {
				$(this).attr("data-hidden", 'true');
			});

			var subElements = $("#page" + id + " ul li[data-hidden!='true']");
			var is_single = false;
			if (subElements.length <= 0) {
				subElements = $("#page" + id + " [data-container='details']");
				is_single = true;
			}
			var index = subElements.length - 1;
			if (index >= 0) {
				var height = $(window).height();
				return SlideDown(subElements, index, is_single);
			}
		}
		return false;
	}

	function SlideDown(subElements, index, is_single) {
		if (is_single) {
			$(subElements).stop().animate({ opacity: 0 }, 800);

			return ShowNextPage();
		}
		if (index >= 0) {
			var changedElement = subElements[index];

			$(changedElement).stop().animate({ 'top': $(window).height() - $(changedElement).offset().top + "px", opacity: 0 }, 300, function () {
				$(this).attr("data-hidden", 'true');
				if ((index - 1) >= 0) {
					return SlideDown(subElements, index - 1, is_single);
				}
			});
			if (index == 0) {
				return ShowNextPage();
			}
		}
	}

	function ShowNextPage() {
		$("#page" + currentPage).stop().animate({ opacity: 0 }, 1700, function () {
		});
		currentPage++;
		if (currentPage > totalPage) {
			$("#main-footer").removeClass('all_footer');
			HideDefaultMenu(1700);
			var padding=$(window).height()-$("#main-footer").height();
			$("#main-footer").css("padding-top",padding);
			$("#main-footer").stop().animate({ 'margin-top': -Math.round($("#main-footer").height() +padding) + "px", opacity: 1 }, 1700, function () { lockScrolling = false; });
			$('#main-footer').on('DOMMouseScroll mousewheel', function (event) {
				if (!lockScrolling && $('#main-footer').length > 0 && $('#main-footer').css('opacity')=="1") {

					if (event.originalEvent.detail > 0 || event.originalEvent.wheelDelta < 0) {

					} else {
						lockScrolling = true;
						if (!$("#main-footer").hasClass('all_footer')) {
							ShowDefaultMenu();
							$("#main-footer").stop().animate({ 'margin-top': "0px", opacity: 0 }, 1700, function() {
								$("#main-footer").addClass('all_footer');
								currentPage--;
								scrollUpProcess(currentPage)
							});
						}
					}
				}
			});
		} else {
			SetBullet(currentPage);
			if ($("#page" + currentPage).find('h1[data-hidden="true"]').length > 0) {
				$("#page" + currentPage).find('h1[data-hidden="true"]').css('opacity', '1').css('margin-left', '0').attr('data-hidden','');
				$("#page" + currentPage).find('[data-container="details"]').css('opacity', '1');
			}

			$("#page" + currentPage).stop().animate({ opacity: 1 }, 1700, function () { lockScrolling = false;  });
		}
	}

	function scrollUpProcess(id) {
		if ($('#main-footer').length > 0 && $('#main-footer').css('opacity') == "1") {
			$('#main-footer').off('DOMMouseScroll mousewheel');
		}
		if ($(window).scrollTop() == 0 && $("#page" + id).length <= 0) {
			currentPage = 1;

			lockScrolling = false;
			return true;
		} else {
			lockScrolling = true;
		}
		SetBullet(currentPage);
		$("#page" + currentPage).stop().animate({ opacity: 1 }, 1700,function() {
			
			lockScrolling = false;
		});
		var subElements = $("#page" + id + " ul li[data-hidden='true']");
		var index = 0;
		var is_single = false;
		if (subElements.length <= 0) {
			subElements = $("#page" + id + " [data-container='details']");
			is_single = true;
		}
		ScroolUp(subElements, index, is_single)

		if ($("#page" + id + " h1").attr("data-hidden") != 'false') {
			$("#page" + id + " h1").stop().animate({ 'margin-left': $("#page" + id + " h1").attr("data-first") + "px", opacity: 1 }, 1700, function () {
				$(this).attr("data-hidden", 'false');
			});
		}
		else {
			return true;
		}
		return false;
	}

	function ScroolUp(subElements, index, id, is_single) {
		if (is_single) {
			$(subElements).stop().animate({ opacity: 1 }, 800, function () {
				if ($("#page" + id + " h1").attr("data-hidden") != 'false') {
					$("#page" + id + " h1").stop().animate({ 'margin-left': $("#page" + id + " h1").attr("data-first") + "px", opacity: 1 }, 1700, function () {
						$(this).attr("data-hidden", 'false');
						lockScrolling = false;
					});
					//lockScrolling = false;
					return true;
				}
			});
		}
		if (subElements.length > 0) {
			var height = $(window).height();
			var changedElement = subElements[index];
			$(changedElement).stop().animate({ 'top': "0px", opacity: 1 }, 300, function () {
				$(this).attr("data-hidden", 'false');
				if (index + 1 < subElements.length) {
					return ScroolUp(subElements, index + 1, id, is_single);
				} else {
					if ($("#page" + id + " h1").attr("data-hidden") != 'false') {
						$("#page" + id + " h1").stop().animate({ 'margin-left': $("#page" + id + " h1").attr("data-first") + "px", opacity: 1 }, 1700, function () {
							$(this).attr("data-hidden", 'false');
							lockScrolling = false;
						});
						//lockScrolling = false;
						return true;
					}
					//lockScrolling = false;
				}
			});
		}
	}

	function BlurElement(element, value) {
		element.css('-webkit-filter', 'blur(' + value + 'px)');
		element.css('-moz-filter', 'blur(' + value + 'px)');
		element.css('-o-filter', 'blur(' + value + 'px)');
		element.css('-ms-filter', 'blur(' + value + 'px)');
		element.css('filter', 'blur(' + value + 'px)');
	}

	function ShowDefaultMenu() {
		$('.menu_default .blc_top').stop().animate({ 'margin-top': "0px" }, 1700);
	}

	function HideDefaultMenu(speed,callbackFnk) {
		$('.menu_default .blc_top').stop().animate({ 'margin-top': -($('.menu_default .blc_top').height()) + "px" }, speed, function () {
			if(typeof callbackFnk == 'function'){
				callbackFnk.call(this);
			}
		});
	}
	function SetBullet(id) {
		$('[id^="bullet"]').each(function() {
			$(this).removeClass('active');
		});
		$('#bullet' + id).addClass('active');
	}
});