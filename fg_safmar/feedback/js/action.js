jQuery(document).ready(function () {


CheckSubmenuPosition();
//CloseAllContent();
  var topLogo = $('.logo').innerHeight() + 30;
    jQuery(window).scroll(function(){
		if(jQuery(window).scrollTop() >= topLogo){
			$('.logo').fadeOut(200);
			//jQuery('.logo_fix').addClass('active');
			$('.logo_fix').fadeIn(200);
		} 
        else {
        	//jQuery('.logo_fix').removeClass('active'); 
			$('.logo').fadeIn(200);
			$('.logo_fix').fadeOut(200);
        }  
		CheckSubmenuPosition();
    });	
	
	$('.ver_tabs li').unbind("click").bind("click",function(){		
		if($(this).is('.open')){
			CloseContent($(this));
		}else{
			OpenContent($(this));
		}		
	});
	$("#submenu-support .hor_tabs li").unbind("click").bind("click",function(){
		if($(this).attr('data-target')!="all-services")
		{
			//$("#submenu-support .hor_tabs li").removeClass("active");
			if($(this).hasClass("active"))
			{
				$(this).removeClass("active")
			}else
			{
				$("#submenu-support .hor_tabs li[data-target='all-services']").removeClass("active");
				$(this).addClass("active");
			}
			
			var element=$("#"+$(this).attr('data-target'));
			
			if($(this).hasClass("active"))
			{				
				$("#submenu-support .hor_tabs li:not(.active)").each(function(){
					CloseContent($("#"+$(this).attr('data-target')));
				});
				OpenContent(element);
			}else
			{
				CloseContent(element);
			}
			setTimeout(function(){
			var additional=$('#submenu-support').innerHeight()+$('.blc_bg_menu').innerHeight()+25;
			$.scrollTo(element.offset().top-additional, 500);	
			}, 400);

			
			//$.scrollTo(element.position().top+additional, 500);
			
		}else
		{
			var state=$(this).hasClass("active");
			
			$("#submenu-support .hor_tabs li").removeClass("active");
			
			if(state)
			{
				$(this).removeClass("active")
			}else
			{
				$(this).addClass("active");
			}
			var additional=$('#submenu-support').innerHeight()+$('.blc_bg_menu').innerHeight()+25;
			$.scrollTo($(".ver_tabs").offset().top-additional, 500);
			if($(this).hasClass("active"))
			{
				OpenAllContent();
			}else
			{
				CloseAllContent();
			}
		}
	});
	
	function CloseAllContent()
	{
		$('.ver_tabs li').each(function(){
			CloseContent($(this));
		});
	}
	function OpenAllContent()
	{
		$('.ver_tabs li').each(function(){			
			OpenContent($(this));
			if($(this).hasClass('open')){
				OpenContent($(this));
			}
		});
	}
	
	function OpenContent(element)
	{
		if(!element.find('.direction').hasClass('active')){
			//$('.hor_tabs li:first-child').addClass('active_plus');
			element.toggleClass('active');
			element.find('.direction').addClass('active');
			element.closest('li').toggleClass('active');
			element.closest('li').find('.desc').slideToggle();
			//$('.hor_tabs li:last-child').toggleClass('active');			
		}
	}
	function CloseContent(element)
	{
		if(element.find('.direction').hasClass('active')){
			element.removeClass('open');
			element.find('.desc').slideUp();
			//$(this).closest('li').find('.desc').slideDown();
			//$('.hor_tabs li:last-child').addClass('active');
			//$('.hor_tabs li:first-child').removeClass('active');
			element.find('.direction').removeClass('active');
			//$(this).addClass('active');	
			//$('.hor_tabs li:first-child').addClass('active_plus');
		}
	}
	
	function CheckSubmenuPosition()
	{
		if($('#submenu-support').length>0)
		{
			var topPosition=$('#submenu-support').offset().top;
			if($('#submenu-support').attr("data-top")!="0")
			{
				topPosition=$('#submenu-support').attr("data-top");
			}
			var topPositionValue=parseInt(topPosition);
			if($(window).scrollTop()+100>=topPositionValue)
			{
				if($('#submenu-support').attr("data-top")=="0")
				{
					$('#submenu-support').attr("data-top",$('#submenu-support').offset().top);			
					$('#submenu-support').parent("div").css("margin-top","120px");
				}
				$('#submenu-support').addClass("hor_tabs_fix");
				$('#submenu-support').css("margin-top","70px");
			}
			
			else
			{
				if($('#submenu-support').attr("data-top")!="0")
				{
					$('#submenu-support').attr("data-top","0");
					$('#submenu-support').parent("div").css("margin-top","auto");
				}
				$('#submenu-support').removeClass("hor_tabs_fix");
				$('#submenu-support').css("margin-top","auto");
			}
		}
	}
	
	var listLi = $('ul.all_pack li:first-child ul.charter li, .pensioner_admin_folder_main ul li').length;
	if( listLi > 4 ){
		$('ul.all_pack li:first-child ul.charter, .pensioner_admin_folder_main ul').addClass('lot_of');
	} else{
		$('ul.all_pack li:first-child ul.charter, .pensioner_admin_folder_main ul').removeClass('lot_of');
	}
	
	
	//$(".fancybox").fancybox();
	
    $("select").chosen({
		//"disable_search": true, //поле поиска выключено
		"disable_search": true, //поле поиска включено
		width: '100%'
	});
	
	$('.chosen-results').niceScroll({
		horizrailenabled: false,
		autohidemode: false,
		cursorfixedheight: 20
	});	
	
	
	(function($) {
		$(function() {
			$('ul.part, .ftabs').on('click', 'li:not(.active)', function() {
				$(this)
					.addClass('active').siblings().removeClass('active')
					.closest('div.tabs, .blc_ftabs').find('div.tabs__content, .blc_ftabs_cnt').removeClass('active').eq($(this).index()).addClass('active');
			});
		});
	})(jQuery);	
	
	if($('input').is('.phone')){
		$('input.phone').mask('+7 (999) 999-99-99');
		jQuery.validator.addMethod('phoneRU', function(value, element) {
			return this.optional(element) || /^\+7 \(([0-9]){3}\) ([0-9]){3}-([0-9]){2}-([0-9]){2}$/.test(value) || /^([0-9]){10}$/.test(value);
		}, '');
		var form = $('#feedback form');
		form.validate({
			errorClass : 'error',
			invalidHandler: function(event, validator) {
				//console.log(validator);
			},
			errorPlacement: function(error, element) {
				// error.appendTo( element.closest(''));
			},
			highlight: function(element, errorClass, validClass) {
				$(element).addClass('error').removeClass('valid');
			},
			unhighlight: function(element, errorClass, validClass) {
				$(element).removeClass('error').addClass('valid');
			},
			rules: {
				name: {
					required: true,
					minlength: 2
				},
				phone: {
					required: true,
					phoneRU: true
				},
				time: {
					required: true
				},
				email: {
					required: true,
					email: true
				}
			}
		});	
	}


	$('input[type="number"]').css({
	//    left: 40,
	//    position: 'relative'
	}).after(function() {
		return $('<div />', {
			'class': 'spinner_b',
	//        css : {
	//            height: $(this).outerHeight(),
	//            top: $(this).position().top,
	//            left: $(this).position().left - 40,
	//        },
			//text: '-'
			text: ''
		}).on('click', {input : this}, function(e) {
			e.data.input.value = (+e.data.input.value) - 1;
			$(this).closest('.input').addClass('input--filled');
			$('.spinner_t').removeClass('active');
			$(this).addClass('active');
		});
	}).before(function() {
		return $('<div />', {
			'class': 'spinner_t',
			css : {
	//            height: $(this).outerHeight(),
	//            top: $(this).position().top,
	//            left: $(this).position().left + $(this).width(),
			},
			//text: '+'
			text: ''
		}).on('click', {input : this}, function(e) {
			e.data.input.value = (+e.data.input.value) + 1;
			$(this).closest('.input').addClass('input--filled');
			$('.spinner_b').removeClass('active');
			$(this).addClass('active');
		});
	});

	
	
	
	
	
	$('input.number').css({
	//    left: 40,
	//    position: 'relative'
	}).after(function() {
		return $('<div />', {
			'class': 'spinner_b',
	//        css : {
	//            height: $(this).outerHeight(),
	//            top: $(this).position().top,
	//            left: $(this).position().left - 40,
	//        },
			//text: '-'
			text: ''
		}).on('click', {input : this}, function(e) {
			//e.data.input.value = 1;
						
			if(e.data.input.value < 0){
				e.data.input.value = (+e.data.input.value) + 1;
			}else if (e.data.input.value > 0){
				e.data.input.value = (+e.data.input.value) - 1;
			}else if (e.data.input.value = 0){
				e.data.input.value = 1;
			}
			$(this).closest('.input').addClass('input--filled');
			$('.spinner_t').removeClass('active');
			$(this).addClass('active');
		});
	}).before(function() {
		return $('<div />', {
			'class': 'spinner_t',
			css : {
	//            height: $(this).outerHeight(),
	//            top: $(this).position().top,
	//            left: $(this).position().left + $(this).width(),
			},
			//text: '+'
			text: ''
		}).on('click', {input : this}, function(e) {
			e.data.input.value = (+e.data.input.value) + 1;
			$(this).closest('.input').addClass('input--filled');
			$('.spinner_b').removeClass('active');
			$(this).addClass('active');
		});
	});
	
	
	
	$('input[type="number"]').blur(function(){
		$('.spinner_t, .spinner_b').removeClass('active');
	})

	

	window.dzsscr_init($('body'),{
		'type':'scrollTop'
		,'settings_skin':'skin_apple'
		,enable_easing: 'on'
		,settings_autoresizescrollbar: 'on'
		,settings_chrome_multiplier : 0.04
	})
	
	
	

	
	function masc(){
		///$.mask.definitions['d']='[0-9]';
		$("#input-13, #input-14, #input-18, #input-19").mask("9999");	
	}
	masc();
	
	function passport(){
		$.mask.definitions['h']='[A-Fa-f0-9]';
		$("#input-4").mask("hhhh hhhhhh");
	}
	passport();
	
	function kod(){
		$("#input-6").mask("999 999");
	}
	kod();	
	
	function snils(){
		$("#input-7").mask("999 999999 99");
	}
	snils();	
		
	function index_city(){
		$("#input-10, #input-15").mask("999999");
	}
	index_city();		

	
			
	$(function() {
		$( ".datepicker" ).datepicker({
			language: 'ru',
			dateFormat: 'dd.mm.yy',
			prevText: '',
			nextText: '',			
			showAnim: 'slideDown',
			onSelect: function() {
					$('#input-5').parent('.input').addClass('input--filled');
				}			
		});
	});
	
 
	
	//	var myWidth = 0;
	//	myWidth = window.innerWidth;
	//	jQuery('body').prepend('<div id="size" style="background:#000;padding:5px;position:fixed;z-index:99999;color:#fff;">Width = '+myWidth+'</div>');
	//	jQuery(window).resize(function(){
	//		var myWidth = 0;
	//		myWidth = window.innerWidth;
	//		jQuery('#size').remove();
	//		jQuery('body').prepend('<div id="size" style="background:#000;padding:5px;position:fixed;z-index:99999;color:#fff;">Width = '+myWidth+'</div>');
	//	});	


	InitFeedbackSteps();

	//$('[data-required="step1"]')
	function InitFeedbackSteps() {
		var feeledPropertiesStep2 = 0;
		$('[data-required="step2"]').each(function() {
			if (!!$(this).val()) {
				feeledPropertiesStep2++;
			}
		});
		HideStep($('div.step3'));

		HideStep($('div.step2_2'));

		if (feeledPropertiesStep2 > 0) {
			ShowStep2();
		} else {
			HideStep($('div.step2'));
		}

		$('[data-required="step1"]').on("keyup", function () {
			if (CheckStep1Feeling()) {
				AnimatedShowStep($('div.step2'));
			} else {
				AnimatedHideStep($('div.step2'));
			}
		});

		$('#radio1').click(function () {
			if ($('#radio2').is(":checked") && $('#radio4').is(":checked")) {
				AnimatedShowStep($('div.step2_2'));
			} else {
				AnimatedHideStep($('div.step2_2'));
			}
		});
		$('#radio2').click(function() {
			if ($(this).is(":checked") && $('#radio4').is(":checked")) {
				AnimatedShowStep($('div.step2_2'));
			} else {
				AnimatedHideStep($('div.step2_2'));
			}
		});

		$('#radio3').click(function () {
			if ($('#radio4').is(":checked") && $('#radio2').is(":checked")) {
				AnimatedShowStep($('div.step2_2'));
			} else {
				AnimatedHideStep($('div.step2_2'));
			}
		});
		$('#radio4').click(function() {
			if ($(this).is(":checked") && $('#radio2').is(":checked")) {
				AnimatedShowStep($('div.step2_2'));
			} else {
				AnimatedHideStep($('div.step2_2'));
			}
		});

		$('[data-required="step2"]').on("keyup", function () {
			if (CheckStep2Feeling($('div.step3'))) {
				AnimatedShowStep($('div.step3'));
			}
		});
	}

	function CheckStep1Feeling()
	{
		var feeledPropertiesStep1 = 0;
		var countRequiredElements= $('[data-required="step1"]').length;
		$('[data-required="step1"]').each(function () {
			if (!!$(this).val()) {
				feeledPropertiesStep1++;
			}
		});
		if (feeledPropertiesStep1 == countRequiredElements) {
			return true;
		}
		return false;
	}

	function CheckStep2Feeling()
	{
		var feeledPropertiesStep2 = 0;
		var countRequiredElements= $('[data-required="step2"]').length;
		$('[data-required="step2"]').each(function () {
			if (!!$(this).val()) {
				feeledPropertiesStep2++;
			}
		});
		if (feeledPropertiesStep2 == countRequiredElements-2) {
			return true;
		}
		return false;
	}

	function ShowStep2() {
		var height = $('div.step2').attr('data-height');
		if (!!height) {
			$('div.step2').css('display', 'block').css('height', height);
		}
	}

	function AnimatedShowStep(element) {
		var height = element.attr('data-height');
		if (!!height) {
			element.css('display', 'block');
			element.stop().animate({ 'height': height }, {
				easing: "linear",
				duration: 800
			});
		}
	}

	function AnimatedHideStep2() {
		var height = $('div.step2').attr('data-height');
		if (!!!height) {
			height = $('div.step2').height();
		}
		$('div.step2').attr('data-height', height);
		$('div.step2').stop().animate({ 'height': 0 }, {
				easing: "linear",
				duration: 800,
				complete: function () {
					$('div.step2').css('display', 'none');
				}
			});
	}

	function AnimatedHideStep(element) {
		var height = element.attr('data-height');
		if (!!!height) {
			height = element.height();
		}
		element.attr('data-height', height);
		element.stop().animate({ 'height': 0 }, {
				easing: "linear",
				duration: 800,
				complete: function () {
					element.css('display', 'none');
				}
			});
	}

	function HideStep(element) {
		var height = element.attr('data-height');
		if (!!!height) {
			height = element.height();
		}
		element.attr('data-height', parseInt(height));
		element.css('display', 'none').css('height', '0');
	}

});
