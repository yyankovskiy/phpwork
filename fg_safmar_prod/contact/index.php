<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("h1", "Присоединяйтесь");
$APPLICATION->SetTitle("Сафмар");
?>

<div class="pensioner_contact">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
				<h2 class="vs">Наши<br>контакты</h2>
				<h2 class="hd">Наши контакты</h2>
				<h5 class="vs">Профессиональное администрирование деятельности</h5>
				<h5 class="hd">Профессиональное администрирование деятельности</h5>
				<p class="vs">Пенсионный админстратор САФМАР специализируется на профессиональном администрировании и оказании услуги по </p>
				<p class="hd">Пенсионный админстратор САФМАР специализируется на профессиональном администрировании и оказании услуги по </p>
			</div>
			<div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
				<ul class="cnt">
					<li>
						<div class="desc">
							<p class="green">8 800 200 00 00</p>
							<p class="vs">с 9.00 до 18.00 по<br>Московскому времени</p>
							<p class="hd">с 9.00 до 18.00 по Московскому времени</p>
						</div>								

					</li>
					<li>
						<div class="desc">
							<p class="green">Написать нам</p>
							<p><a href="mailto:cs@safmarpensions.ru">cs@safmarpensions.ru</a></p>
						</div>
					</li>
					<li>
						<div class="desc">
							<p class="green">Адрес</p>
							<p class="vs">109240, Россия, г. Москва,<br>ул. Верхняя Радищевская,<br>дом 16, строение 2</p>
							<p class="hd">109240, Россия, г. Москва, ул. Верхняя Радищевская, дом 16, строение 2</p>
						</div>								

					</li>
					<li>
						<div class="desc">
							<p class="green">Обратная связь</p>
							<p class="vs">Получить данные<br>по своему пенсионному<br>счету </p>
							<p class="hd">Получить данные по своему пенсионному счету </p>
						</div>	
					</li>
				</ul>
			</div>												
		</div>
	
	</div><!--container-->
</div>

<div class="product_details_map">
	<div class="map-canvas" id="map10"></div>
	<span name='' text='Сафмар' url='#' x='37.6302766'  y='55.732798'></span>
</div>

<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>