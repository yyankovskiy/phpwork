jQuery(document).ready(function () {


CheckSubmenuPosition();
//CloseAllContent();
  var topLogo = $('.logo').innerHeight() + 30;
      jQuery(window).scroll(function(){
		if(jQuery(window).scrollTop() >= topLogo){
			jQuery('.logo_fix').addClass('active'); 
		} 
        else {
        	jQuery('.logo_fix').removeClass('active'); 
        }  
		CheckSubmenuPosition();
    });
	
	$('.ver_tabs li').unbind("click").bind("click",function(){		
		if($(this).is('.open')){
			CloseContent($(this));
		}else{
			OpenContent($(this));
		}		
	});
	$("#submenu-support .hor_tabs li").unbind("click").bind("click",function(){
		if($(this).attr('data-target')!="all-services")
		{
			//$("#submenu-support .hor_tabs li").removeClass("active");
			if($(this).hasClass("active"))
			{
				$(this).removeClass("active")
			}else
			{
				$("#submenu-support .hor_tabs li[data-target='all-services']").removeClass("active");
				$(this).addClass("active");
			}
			
			var element=$("#"+$(this).attr('data-target'));
			
			if($(this).hasClass("active"))
			{				
				$("#submenu-support .hor_tabs li:not(.active)").each(function(){
					CloseContent($("#"+$(this).attr('data-target')));
				});
				OpenContent(element);
			}else
			{
				CloseContent(element);
			}
			setTimeout(function(){
			var additional=$('#submenu-support').innerHeight()+$('.blc_bg_menu').innerHeight()+25;
			$.scrollTo(element.offset().top-additional, 500);	
			}, 400);

			
			//$.scrollTo(element.position().top+additional, 500);
			
		}else
		{
			var state=$(this).hasClass("active");
			
			$("#submenu-support .hor_tabs li").removeClass("active");
			
			if(state)
			{
				$(this).removeClass("active")
			}else
			{
				$(this).addClass("active");
			}
			var additional=$('#submenu-support').innerHeight()+$('.blc_bg_menu').innerHeight()+25;
			$.scrollTo($(".ver_tabs").offset().top-additional, 500);
			if($(this).hasClass("active"))
			{
				OpenAllContent();
			}else
			{
				CloseAllContent();
			}
		}
	});
	
	function CloseAllContent()
	{
		$('.ver_tabs li').each(function(){
			CloseContent($(this));
		});
	}
	function OpenAllContent()
	{
		$('.ver_tabs li').each(function(){			
			OpenContent($(this));
			if($(this).hasClass('open')){
				OpenContent($(this));
			}
		});
	}
	
	function OpenContent(element)
	{
		if(!element.find('.direction').hasClass('active')){
			//$('.hor_tabs li:first-child').addClass('active_plus');
			element.toggleClass('active');
			element.find('.direction').addClass('active');
			element.closest('li').toggleClass('active');
			element.closest('li').find('.desc').slideToggle();
			//$('.hor_tabs li:last-child').toggleClass('active');			
		}
	}
	function CloseContent(element)
	{
		if(element.find('.direction').hasClass('active')){
			element.removeClass('open');
			element.find('.desc').slideUp();
			//$(this).closest('li').find('.desc').slideDown();
			//$('.hor_tabs li:last-child').addClass('active');
			//$('.hor_tabs li:first-child').removeClass('active');
			element.find('.direction').removeClass('active');
			//$(this).addClass('active');	
			//$('.hor_tabs li:first-child').addClass('active_plus');
		}
	}
	
	function CheckSubmenuPosition()
	{
		if($('#submenu-support').length>0)
		{
			var topPosition=$('#submenu-support').offset().top;
			if($('#submenu-support').attr("data-top")!="0")
			{
				topPosition=$('#submenu-support').attr("data-top");
			}
			var topPositionValue=parseInt(topPosition);
			if($(window).scrollTop()+100>=topPositionValue)
			{
				if($('#submenu-support').attr("data-top")=="0")
				{
					$('#submenu-support').attr("data-top",$('#submenu-support').offset().top);			
					$('#submenu-support').parent("div").css("margin-top","120px");
				}
				$('#submenu-support').addClass("hor_tabs_fix");
				$('#submenu-support').css("margin-top","70px");
			}
			
			else
			{
				if($('#submenu-support').attr("data-top")!="0")
				{
					$('#submenu-support').attr("data-top","0");
					$('#submenu-support').parent("div").css("margin-top","auto");
				}
				$('#submenu-support').removeClass("hor_tabs_fix");
				$('#submenu-support').css("margin-top","auto");
			}
		}
	}
	
	var listLi = $('ul.all_pack li:first-child ul.charter li, .pensioner_admin_folder_main ul li').length;
	if( listLi > 4 ){
		$('ul.all_pack li:first-child ul.charter, .pensioner_admin_folder_main ul').addClass('lot_of');
	} else{
		$('ul.all_pack li:first-child ul.charter, .pensioner_admin_folder_main ul').removeClass('lot_of');
	}
	
	
	//$(".fancybox").fancybox();
	
//    $("select").chosen({
//		//"disable_search": true, //поле поиска выключено
//		"disable_search": false, //поле поиска включено
//		width: '100%'
//	});
	
	
	(function($) {
		$(function() {
			$('ul.part').on('click', 'li:not(.active)', function() {
				$(this)
					.addClass('active').siblings().removeClass('active')
					.closest('div.tabs').find('div.tabs__content').removeClass('active').eq($(this).index()).addClass('active');
			});
		});
	})(jQuery);	
	
//	var myWidth = 0;
//	myWidth = window.innerWidth;
//	jQuery('body').prepend('<div id="size" style="background:#000;padding:5px;position:fixed;z-index:99999;color:#fff;">Width = '+myWidth+'</div>');
//	jQuery(window).resize(function(){
//		var myWidth = 0;
//		myWidth = window.innerWidth;
//		jQuery('#size').remove();
//		jQuery('body').prepend('<div id="size" style="background:#000;padding:5px;position:fixed;z-index:99999;color:#fff;">Width = '+myWidth+'</div>');
//	});	
	
	
});



