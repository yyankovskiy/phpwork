<?if(!CModule::IncludeModule("iblock"))
    return;?>

    <?
    $uri = explode("/", $_SERVER['REQUEST_URI']);
    if($uri[1] === "services" || $uri[1] === "partners" || $uri[1] === "contact")
        $class_name = "bg_".$uri[1];
    else
        $class_name = "bg_main";
    ?>
    <div id="header" class="<?=$class_name?>">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="logo"><a href="/"><img src="/upload/img/logo.png" alt=""></a></div>
                </div>
            </div>  
        </div><!--container-->
    </div><!--header-->
    <div class="progress_bar"></div> <!--fix_panel-->
    <div class="blc_bg_menu">
        <div class="bg_menu">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="navbar navbar-default navbar-static-top">
                            <div class="navbar-header">
                                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                            </div>
                            <div class="navbar-collapse collapse">
                                <div class="logo_fix"><a href="/"><img src="/upload/img/logo_fix.png" alt=""></a></div>
                                <?$APPLICATION->IncludeComponent("bitrix:menu", "main_menu", Array(
                                    "ROOT_MENU_TYPE" => "top",  // Тип меню для первого уровня
                                    "MAX_LEVEL" => "0", // Уровень вложенности меню
                                ), false );?>
                            </div><!--/.nav-collapse -->    
                        </div>                  
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="offset"></div>


		