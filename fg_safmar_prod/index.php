<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("h1", "Создавайте уверенное будущее");
$APPLICATION->SetTitle("Сафмар");
?>
<div class="pensioner_admin">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
				<h2>Пенсионный администратор</h2>
				<h6>Профессиональное администрирование деятельности негосударственных пенсионных фондов</h6>
				<p>Пенсионный админстратор САФМАР специализируется на профессиональном администрировании и оказании услуги по организационному, информационному и техническому обеспечению деятельности негосударственных пенсионных фондов, включая услуги по разработке и внедрению корпоративных пенсионных программ, ведению пенсионных счетов и проч.</p>	
			</div>
			<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
				<h2>Политика конфеденциальности</h2>
				<h6 class="vs">Конфеденциальности и безопасность<br>хранения данных</h6>
				<h6 class="hd">Конфеденциальности и безопасность хранения данных</h6>
				<p>Пенсионный админстратор САФМАР гарантирует обеспечение конфиденциальности данных, полученных в ходе окозания услуг, надежности и безопасности хранения информации. Любые сведения, касающиеся, в частности, пенсионных счетов граждан, хранятся в строгой тайне, не передаются и не разглашаются без писменного соглашения второй стороны.</p>
			</div>
		</div>				
	</div><!--container-->
</div>

<div class="pensioner_admin_circle_main">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
				<h2>Пенсионный администратор</h2>
			</div>
		</div>					
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<ul class="number_participants">
					<li>
						<div class="desc">
							<p>Количество участников, <span>чел</span></p>
							<p class="number">30 998</p>
							<div class="crc"></div>
						</div>
					</li>
					<li>
						<div class="desc">
							<p>Количество застрахованных лиц, <span>чел</span></p>
							<p class="number">1 710 538</p>
							<div class="crc"></div>
						</div>									
					</li>
					<li>
						<div class="desc">
							<p>Имущество фонда, <span>чел</span></p>
							<p class="number">94 350 793</p>
							<div class="crc"></div>
						</div>									
					</li>
				</ul>
			</div>
		</div>
	</div><!--container-->	
</div>

<div class="pensioner_admin_folder_main">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
				<h2>Пенсионный администратор</h2>
			</div>
		</div>
		<?$APPLICATION->IncludeComponent( "bitrix:news.list", "about_company", Array(
			"ACTIVE_DATE_FORMAT" => "d.m.Y",
			"ADD_SECTIONS_CHAIN" => "Y",
			"AJAX_MODE" => "N",
			"AJAX_OPTION_ADDITIONAL" => "",
			"AJAX_OPTION_HISTORY" => "N",
			"AJAX_OPTION_JUMP" => "N",
			"AJAX_OPTION_STYLE" => "Y",
			"CACHE_FILTER" => "N",
			"CACHE_GROUPS" => "Y",
			"CACHE_TIME" => "36000000",
			"CACHE_TYPE" => "A",
			"CHECK_DATES" => "Y",
			"COMPONENT_TEMPLATE" => "about_company",
			"DETAIL_URL" => "",
			"DISPLAY_BOTTOM_PAGER" => "Y",
			"DISPLAY_DATE" => "N",
			"DISPLAY_NAME" => "Y",
			"DISPLAY_PICTURE" => "N",
			"DISPLAY_PREVIEW_TEXT" => "Y",
			"DISPLAY_TOP_PAGER" => "N",
			"FIELD_CODE" => array("",""),
			"FILTER_NAME" => "",
			"HIDE_LINK_WHEN_NO_DETAIL" => "N",
			"IBLOCK_ID" => "1",
			"IBLOCK_TYPE" => "CONTENT",
			"INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
			"INCLUDE_SUBSECTIONS" => "N",
			"MESSAGE_404" => "",
			"NEWS_COUNT" => "20",
			"PAGER_BASE_LINK_ENABLE" => "N",
			"PAGER_DESC_NUMBERING" => "N",
			"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
			"PAGER_SHOW_ALL" => "N",
			"PAGER_SHOW_ALWAYS" => "N",
			"PAGER_TEMPLATE" => ".default",
			"PAGER_TITLE" => "Новости",
			"PARENT_SECTION" => "",
			"PARENT_SECTION_CODE" => "",
			"PREVIEW_TRUNCATE_LEN" => "",
			"PROPERTY_CODE" => array("","LINK_DOCUMENT",""),
			"SET_BROWSER_TITLE" => "N",
			"SET_LAST_MODIFIED" => "N",
			"SET_META_DESCRIPTION" => "N",
			"SET_META_KEYWORDS" => "N",
			"SET_STATUS_404" => "N",
			"SET_TITLE" => "N",
			"SHOW_404" => "N",
			"SORT_BY1" => "ACTIVE_FROM",
			"SORT_BY2" => "SORT",
			"SORT_ORDER1" => "DESC",
			"SORT_ORDER2" => "DESC"
		) );?>
	</div><!--container-->
</div>
<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>